######################################
Background steps for adding a location
######################################

When a new location is added to the model_verification_input.txt file, an email
is sent to Stef at stephanie@offshoreweather.com.au
This notification can then be followed up with the following processes. 


1. Determine observations data
------------------------------
    Observational data should come from the database.
    Use the verification_scripts/setup_functions.py script to find this data.
    Call the setup_functions.py script with the arguments:
        -l your new location
        
    for example:
    ::
    
        python ./setup_functions.py -l Benchamas 


    Calling this function may take a while (minutes) to return anything, but what is returned will be all of 
    the data specifics which correspond to this location. 
    The result may look similar to this:
    ::

        THIS IS THE LOCATION: Benchamas Wave WITH SITE_ID: 15
        main db read 100.0% done
            variable_id        variable_name      sample_duration       source_id     source_name          source_type
        ---------------------------------------------------------------------------------------------------------------
                     39 significant_wave_height                                   84             wv1          wave_sensor
                     40      max_wave_height                                   84             wv1          wave_sensor
                     41 min_elevation_above_mean                                   84             wv1          wave_sensor
                     42 max_elevation_above_mean                                   84             wv1          wave_sensor
                     43          mean_period                                   84             wv1          wave_sensor
                     44 mean_zero_upcrossing_period                                   84             wv1          wave_sensor
                     45 spectral_peak_period                                   84             wv1          wave_sensor
                     46   significant_period                                   84             wv1          wave_sensor
                     47    wave_energy_total                                   84             wv1          wave_sensor
                     48       peak_frequency                                   84             wv1          wave_sensor
                     49       peak_direction                                   84             wv1          wave_sensor
                     50       mean_direction                                   84             wv1          wave_sensor

    From this output make an note of the site, variable and source id's that are associated with the variable youre interested in. 
    For example, the site, variable and source id's respectively, for the significant_wave_height are 15, 39 and 84.

    The variable and source id's are unique to the location/variable.

    Call the setup_functions.py script again with the arguments:
        -y the year you want to find data for

        -m the month you want to find data for 

        -vid the variable id

        -sid the source id

    for example:
    ::

        python ./setup_functions.py -y 2017 -m 12 -vid 39 -sid 84

    If there is data on the database, the output may look similar to this and a plot should have been produced:
    ::

       Checking if variable_id 39, source_id 84 on database..
       There was data on the database for this variable.
       A plot of this data has been made and is saved in this current location called "test.png"

    Otherwise the result will say: 
    :: 

        No data on the database for this variable.

2. Add the database id to information files.
---------------------------------------------

    Depending on the type of verification youre trying to complete, these id's need to be added to the 
    type of verifications information text file. These files are found on /fs02/info/obs_database_ids
    
    The header of these files outline all the possible variables whose ids can be added. 
    
    Add the variable in the correct column that you would like to extract. If you would like to get more than one, 
    add these ids too. FOR EVERY VARIABLE YOU DONT HAVE THE ID FOR, PUT A ZERO!

    There is an example location where just the tp (peak period) will be extracted in the first line of the wave_database_ids.txt
    
    Please note, that in the weather_database_ids.txt the variable name 'anenometer' may be referred to as the 'optimum_wind_sensor' on the database.

3. Hope for the best...
-----------------------

    Once these steps have been followed, the verification will hopefully work. 
    
    To check that the verification works:
        
        1. Add the location to the file custom_locations.txt
        
        2. Edit the ``custom_run_verification.py`` script with the appropriate verification type, start and end date. 
        
        3. Run this script. The results should be in the results directory.
    
    KNOWN ERRORS:
    
    An import error of the ows_pyobs and ows_pymods modules can occur. To fix this, 
    run source ~/source_development.sh in the command line.  

    An error that may occur is the path to the model forecast files on Archive1 may be incorrect. 
    If this is the case, the basin may need to be added to the information.py files OR the locations name and model point you entered may be different to that in the file name. 

    
