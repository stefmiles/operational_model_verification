.. Model verifications documentation master file, created by
   sphinx-quickstart on Fri Dec 22 02:19:39 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Model verifications's documentation!
===============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   adding_location.rst
   manual_steps.rst
   intro.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
