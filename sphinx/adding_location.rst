#####################
Adding a new location
#####################

1. Add location to file
-----------------------
    To add a new location to be verified, the location and its model point needs to be added to the file
    model_verification_input.txt in the fs02/forecast_points directory. 
        ** Note: longitude needs to be 3 numbers long and latitude, 2 numbers. See how Satun is written. 
    
    Once this location has been added, Stef will get an email and complete the few more steps needed to complete the process. 

    These steps are documented `here <http://192.168.57.178/model_verifications/manual_steps.html>`_  if Stef is unavailable. 

