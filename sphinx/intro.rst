########################
Specifics of the process
########################

====
What
====
A monthly verification is attempted for each of the five verification types:
    1. WaveWatch3 (Sig Wave height(hs), Peak Period(tp) and Peak Direction(dr)) 
    2. WaveWatch3 Outlook verifications 5-10days out (hs, tp and dr)
    3. WaveWatch3 Outlook verifications 10-15days out (hs, tp and dr)
    4. Global Winds (Wind Speed (wspd) and Wind Direction (wdir))
    5. WRF (wspd, wdir, Precipitation (rain) and Temperature (temp))

====
When 
====
Model verifications are run automatically on the 8th of each month via the cron. 

=====
Where 
=====
All scripts and processes are run on the virtual machine 192.168.57.178/~/Operational/model_verifications/verification_scripts/ 
with all the results being saved in 192.168.57.178/~/Operational/model_verifications/results/verification_type directories.

Some of the results are also copied to the /mnt/ows/Archive1/Verifications/Model_Verifications/verification_type/ 
directories also. These results were copied due to their representation of a certain basin. For example, not all the 
locations from the Gulf of Thailand are copied, only Arthit, Benchamas and Funan will be copied. 

####################
How the process runs
####################
These scripts need a few different files to work. 

=====================
Overview of processes
=====================

These scripts work in a certain order when doing a locations monthly verification, which is:

    1. Observations data is extracted from either a downloaded txt/csv file or the database. 
    2. The data is then sorted into a monthly timeseries array of either 1 or 3 hourly data. 
    3. If observational data exists, statistics are calculated for this data.
    4. If observational data exists, forecasted model data is extracted from model output files on the Archive drive.
    5. Model forecast data is then also sorted into monthly timeseries arrays of either 1 or 3 hourly data depending on the observations frequency and the model. 
    6. If model data exists, statistics are calculated on model vs obs data. 
    7. Model vs obs timeseries, scatter and error plots are then created. 
    8. The annual bias, mae, rmse and rank plots are then created.
    9. Certain locations results (plots and stats) are then copied to the Archive drive and also sent to Stef's email for easy reviewing. 

===========
The process
===========

Firstly:
--------
1. A locations.txt file lists all the locations that model verifications will be calculated for.

   Some of these will be for only one type of verifications, others for all types. 

   This file is checked and edited daily, against the file '/mnt/fs02/fs02/forecast_point/model_verifications_input.txt'

   To add a location to be verified, please see these 
   `notes <http://192.168.57.178/model_verifications/adding_location.html>`_  on how to do so. 

2. An information python file for each verification type is needed. E.g. wave_watch_3_information.py

   These files need to include:
    * The models which are to be included in the verification 
    * The variables which will be calculated
    * The period, start_vp and end_vp which are used to calculate the verification periods (verification periods are similar to taus)
    * The rank, which is the hours after which each models performance is compared to others when calculating the summary statistics
    * The basins in which the model forecast files directories are called. For example, forecasts for Cape Sorell will be found in the BASSX basin/directories on the Archive drive. 

3. A database information text file is needed. 

   These files are used to get the observations data from the database by supplying the variable and source id's
   for each location and are found in the /mnt/fs02/fs02/info/obs_database_ids/ directories. 
   
   If the id's for a location are not in this file, the observation data may come from a different source. For example, Ichthys hs data is found in the directory 'server1/MOBS/ICHTHYS'

Second:
-------
Most of the modules needed for a verification can be found in the directory/library: 192.168.57.178/~/Operational/model_verifications/verification_scripts. 

These include:
    1. setup_functions.py
    2. verification_top_class.py
    3. observations_class.py 
    4. models_class.py 
    5. statistics_class.py
    6. plotting_class.py
    7. postprocessing_functions

An observations and model class module are also needed. These are packages and can be installed with pip, e.g. pip install ows_pyobs and pip install ows_pymods

Find the documentation for the observations class here: 
`observations_documents <http://192.168.57.178/observations_class/index.html>`_

Third:
------
A verification is run by instantiating a verification object from the 'verification_top_class module', then the function 'run_a_verification' is called.
All statistics and plots are saved in the model_verifications/results directory.

Please read the doc strings from the verification top class below:

.. automodule:: verification_top_class
    :members:
    :show-inheritance:
