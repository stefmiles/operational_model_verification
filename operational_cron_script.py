#!/usr/bin/env python
import os

scripts_path = os.path.join('/home/Operator/Operational','model_verifications',
        'verification_scripts')
run_script = os.path.join(scripts_path, 'verification_top_class.py')
run_log = os.path.join('/home/Operator/Operational', 'model_verifications', 
        'verification_log.txt')

os.system('python ' + run_script + ' >> ' + run_log + ' 2>&1')


#ww3_script = os.path.join(scripts_path, 'wave_watch_3_scripts')
#ww3_log = os.path.join(scripts_path, 'wave_watch_3_scripts', 'ww3log.txt')
#gw_script = os.path.join(scripts_path, 'global_wind_scripts')
#gw_log = os.path.join(scripts_path, 'global_wind_scripts', 'gwlog.txt')
#os.system('python ' + ww3_script + '/ww3_run_script.py >> ' + ww3_log + ' 2>&1')
#os.system('python ' + gw_script + '/gw_run_script.py >> ' + gw_log + ' 2>&1')
