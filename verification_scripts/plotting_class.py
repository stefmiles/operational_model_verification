import matplotlib
matplotlib.use('Agg')

import os

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from datetime import datetime
import scipy.stats
from dateutil.relativedelta import relativedelta
from datetime import timedelta

import ows_line_styles as linestyles

class Plotting(object):
    def __init__(self, observations_instance, 
            location='', 
            save_path='',
            start_date=None,
            end_date=None,
            model_dictionary={}, 
            verification_periods=[0], 
            variables=['hs'], 
            verification_type=None,
            models=[]):
        self.observations_instance = observations_instance
        self.location = location 
        self.save_path = save_path
        if start_date:
            self.start_date = start_date
        if end_date:
            self.end_date = end_date
        if model_dictionary:
            self.model_dictionary = model_dictionary
        self.verification_periods = verification_periods
        self.variables = variables
        if verification_type:
            self.veri_type = verification_type
            if self.veri_type != 'wrf':
                self.dt_increment = 12
            else:
                self.dt_increment = 6
        if models:
            self.models = models


    def determine_no_rows(self, nplots):
        """ Set number of rows in plot depending on number of plots. """
        if nplots == 1:
            nrows = 1
        elif nplots == 2:
            nrows = 1
        elif nplots == 3 or nplots == 4:
            nrows = 2
        elif nplots == 5 or nplots == 6:
            nrows = 3
        elif nplots == 7 or nplots == 8:
            nrows = 4
        elif nplots == 8 or nplots == 9:
            nrows = 5
        else:
            nrows = 10
        return nrows





    def verification_method1(self):
        """ This is the standard model verification function to produce the
        monthly timeseries, errorplots and scatterplots. It also produces the
        summary statistic plots getting data all the way back from 2015. """
        file_date = self.start_date.strftime('%Y%m') 
        path_split = os.path.split(self.save_path)
        upper_save_path = path_split[0]
        self.make_summary_dirs(upper_save_path, self.variables)
        for variable in self.variables:
            print('Making plots for {}'.format(variable))
            for vp in self.verification_periods:
                file_start = '{}_{}_{}-{}_'.format(self.location,
                        variable, vp , vp+self.dt_increment)
                # timeseries
                fig = self.ts_routine_one(variable, vp)
                file_name = file_start + 'timeseries.png'
                png_file = os.path.join(self.save_path, file_date, variable, file_name) 
                if fig:
                    plt.savefig(png_file)
                    plt.close(fig)
                #errorplot
                fig = self.er_routine_one(variable, vp)
                file_name = file_start + 'errorplot.png'
                png_file = os.path.join(self.save_path, file_date, variable, file_name) 
                if fig:
                    plt.savefig(png_file)
                    plt.close(fig)  
                #scatterplot
                fig = self.sc_routine_one(variable, vp)
                file_name = file_start + 'scatterplot.png'
                png_file = os.path.join(self.save_path, file_date, variable, file_name) 
                if fig:
                    plt.savefig(png_file)
                    plt.close(fig)
            # annual plots
            for statistic in [('bias', 2), ('mae', 1), ('rank', 8), ('rmse', 4)]:  
                png_name = '{}_{}_{}_ts.png'.format(self.location, variable,
                        statistic[0])
                png_file = os.path.join(upper_save_path, 
                        '{}_summary'.format(variable), png_name)
                statistic_dict, obs_dict = self.create_annual_stat_dicts(
                        variable, statistic)
                fig = self.scatterplot_annual_data(statistic, variable, 
                        obs_dict, statistic_dict)
                if fig:
                    plt.savefig(png_file)
                    plt.close(fig)

    
    def verification_method2(self):

        """ This is the standard model verification function to produce the
        monthly timeseries, errorplots and scatterplots. It also produces the
        summary statistic plots getting data all the way back from 2015. """
        file_date = self.start_date.strftime('%Y%m') 
        path_split = os.path.split(self.save_path)
        upper_save_path = path_split[0]
        self.make_summary_dirs(upper_save_path, self.variables)
        for variable in self.variables:
            
            print('Making plots for {}'.format(variable))
            for vp in self.verification_periods:
                file_start = '{}_{}_{}-{}_'.format(self.location,
                        variable, vp , vp+self.dt_increment)
                # timeseries
                if variable == 'rain':
                    fig = self.ts_routine_one(variable, vp, rain_var=True)
                else:
                    
                    fig = self.ts_routine_one(variable, vp)
                file_name = file_start + 'timeseries.png'
                png_file = os.path.join(self.save_path, file_date, variable, file_name) 
                if fig:
                    plt.savefig(png_file)
                    plt.close(fig)
                
                #import ipdb;ipdb.set_trace()
                if variable == 'rain':
                    continue
                #errorplot
                fig = self.er_routine_one(variable, vp)
                file_name = file_start + 'errorplot.png'
                png_file = os.path.join(self.save_path, file_date, variable, file_name) 
                if fig:
                    plt.savefig(png_file)
                    plt.close(fig)  
                #scatterplot
                fig = self.sc_routine_one(variable, vp)
                file_name = file_start + 'scatterplot.png'
                png_file = os.path.join(self.save_path, file_date, variable, file_name) 
                if fig:
                    plt.savefig(png_file)
                    plt.close(fig)
            if variable == 'rain':
                continue
            # annual plots
            for statistic in [('bias', 2), ('mae', 1), ('rank', 8), ('rmse', 4)]:  
                
                png_name = '{}_{}_{}_ts.png'.format(self.location, variable,
                        statistic[0])
                png_file = os.path.join(upper_save_path, 
                        '{}_summary'.format(variable), png_name)
                statistic_dict, obs_dict = self.create_annual_stat_dicts(
                        variable, statistic)
                fig = self.scatterplot_annual_data(statistic, variable, 
                        obs_dict, statistic_dict)
                if fig:
                    plt.savefig(png_file)
                    plt.close(fig)




    def make_summary_dirs(self, upper_save_path, variables):
        """ makes the directories for this location for the annual plots. """
        yearmonth = self.start_date.strftime('%Y%m')
        for var in variables:
            complete_png_path = os.path.join(upper_save_path,
                    '{}_summary'.format(var))
            if not os.path.isdir(complete_png_path):
                os.makedirs(os.path.join(complete_png_path))

    def create_months_list(self):
        """ Create a list with monthly datetimes from the start date until the
        specified end date. """
        month_list = []
        my_start_date = datetime(2015,1,1,0) 
        while my_start_date <= self.start_date:
            the_year = my_start_date.year
            the_month = my_start_date.month
            month_list.append(datetime(the_year, the_month, 1))
            my_start_date = my_start_date + relativedelta(months=1)
        return  month_list

    def get_stat_value(self, vp, model, variable, sum_file, stat_col):
        """ Get the statistic value for the verification period(vp), model and
        variable, from the model summary_stats file. """
        summary_file = sum_file 
        sid = open(summary_file, 'r')
        with sid:
            for line in sid:
                if line.count('verification period'):
                    vp = int(line.split()[0].split(':')[0])
                if model in line:
                    if model == 'ensbc':
                        if line.count('ensemble'):
                            continue
                    if model == 'ensemble':
                        if line.count('ensbc'):
                            continue
                    if line.count('Benchamas'):
                        continue
                    if line.count('CORRECTED'):
                        continue
                    if model == 'GFST2':
                        if line.count('T2') !=1 :
                            continue
                    elif model == 'GFS418':
                        if line.count('418') !=1 :
                            continue
                    elif model == 'GFS':
                        if (line.count('T2') or line.count('418')):
                            continue
                    value = float(line.split()[stat_col])

                    return value
        return np.nan
    
    def get_obs_stats(self, obs_path, variable, stat):
        """ Get the raw observations statistic from the obs stats file."""
        oid = open(obs_path, 'r')
        with oid:
            for line in oid:
                if line.count(variable):
                    found_var = True
                if line.count('{}'.format(stat)):
                    obs_percent = line.split()[1]
                    return float(obs_percent)
        return 100

    def create_annual_stat_dicts(self, variable, statistic):
        """ Create both a dictionary of model statistics from each month and 
        observations obtained percentages for each month, for that variable."""
        stat_dict = {}
        ob_stat_dict = {}
        month_list = self.create_months_list()
        file_name = 'summary_stats.txt'
        for month in month_list:
            the_year = month.strftime('%Y')
            the_month = month.strftime('%Y%m')
            ob_file_name = '{}_Obs_Statistics_{}.txt'.format(self.location, the_month)
            upper_summary_path = (os.path.split(self.save_path))[0]
            summary_path = os.path.join(upper_summary_path, the_year,
                    the_month, variable, file_name)
            if os.path.isfile(summary_path):
                stat_dict[the_month] = {}
                if 'ensemble' not in self.models:
                    self.models.insert(len(self.models),'ensemble')
                if 'ensbc' not in self.models:
                    self.models.insert(len(self.models),'ensbc')
                for model in self.models:
                    stat_value = self.get_stat_value(0, model, variable, 
                            summary_path, statistic[1])
                    if not np.isnan(stat_value):
                        stat_dict[the_month][model]=stat_value
            upper_obs_path = (os.path.split(self.save_path))[0]
            obs_sum_path = os.path.join(upper_obs_path,the_year, ob_file_name) 
            if os.path.isfile(obs_sum_path):
                ob_percent = self.get_obs_stats(obs_sum_path, variable, 'obtained')
                if not np.isnan(ob_percent):
                    ob_stat_dict[the_month] = {}
                    ob_stat_dict[the_month] = ob_percent
        return stat_dict, ob_stat_dict


    def make_annual_model_array(self, month_list, model, statistics_dict, 
            obs_dict):
        """ Detemine the stat value for all months for a model. Make it a nan
        if the previous months obs were less than 75%. This takes care of rough
        results for bias correction not being shown. """
        model_data = []
        for index, dt_month in enumerate(month_list):
            the_month = dt_month.strftime('%Y%m')
            try:
                stat_val = statistics_dict[the_month][model]
            except:
                stat_val = np.nan
            if index == 0:
                model_data.append(stat_val)
            else:
                prev_month = month_list[index-1].strftime('%Y%m')
                try:
                    prev_ob = ob_percent = obs_dict[prev_month]
                except:
                    prev_ob = 0
                if prev_ob < 75:
                    model_data.append(np.nan)
                else:
                    model_data.append(stat_val)
        return np.array(model_data)

    def scatterplot_annual_data(self, statistic, variable,  obs_dict, 
            statistic_dict):
        """ Routine to plot all months since 2015 and their stat for each 
        model. """
        fig = None
        month_list = self.create_months_list()
        for model in self.models:
            model_color, model_linestyle = self.get_model_attributes(model)
            model_array = self.make_annual_model_array(month_list, model, 
                    statistic_dict, obs_dict)
            mask = np.isfinite(model_array)
            masked_array = model_array[mask]
            if len(masked_array) > 0:
                if not fig:
                    fig = plt.figure(figsize=(11.69, 8.27))
                    subaxes = fig.add_subplot(111)
                subaxes.scatter(month_list, model_array, color=model_color,
                        label=model)
                subaxes.plot(month_list, model_array, color=model_color,
                        linestyle='--')
                model_min = np.nanmin(model_array)
                model_max = np.nanmax(model_array)
                #subaxes.axhline(model_min, color=model_color, alpha=0.5)
                #subaxes.axhline(model_max, color=model_color, alpha=0.5)
        if fig:
            self.set_annual_x_attributes(subaxes, obs_dict, month_list)
            self.set_annual_y_attributes(subaxes, statistic[0], variable)
            #import ipdb;ipdb.set_trace()
            self.add_model_changes_to_graph(subaxes, month_list)
            subaxes.grid()
            subaxes.legend(fontsize=8, loc='center left', bbox_to_anchor=(1,0.5))
            plt.title('{} {} {}'.format(self.location, variable, statistic[0])) 
        return fig

    def set_annual_x_attributes(self, subaxes, obs_dict, month_list):
        """ Set all of the x details for annual stat plot.""" 
        xticks = [] 
        xlabs = []
        for month in month_list:
            dt_mth = month.strftime('%Y%m')
            try:
                ob_val = obs_dict[dt_mth]
            except:
                ob_val = 0
            if ob_val < 75:
                xlabs.append(month.strftime('**\n%b\n%y\n**'))
            else:
                xlabs.append(month.strftime('%b\n%y'))
            xticks.append(mdates.date2num(month))
        first_xtick = mdates.date2num(month_list[0])
        last_xtick = mdates.date2num(month_list[-1])
        subaxes.set_xlim(first_xtick, last_xtick)
        subaxes.set_xticklabels(xlabs, fontsize=8)
        subaxes.xaxis.set_ticks(xticks)
        subaxes.set_xlabel('Date (UTC) \n Months with astericks, are those '
                + 'with less than 75% of observations data.', fontsize=8)

    def set_annual_y_attributes(self, subaxes, statistic, variable):
        """ Set the y details for the annual statistics plot."""
        if statistic == 'bias':
            subaxes.set_ylim(-0.5,0.7)
        elif statistic == 'rank':
            subaxes.set_ylim(0,1)
        else:
            subaxes.set_ylim(-1,1)
        #if variable == 'tp':
        #    subaxes.set_ylim(-20,20)
        #if variable == 'dr':
        #    subaxes.set_ylim(-90,90)
        #if variable == 'wdir':
        #    subaxes.set_ylim(-90,90)
        #if variable == 'wspd':
        #    subaxes.set_ylim(-5,5)
        #if statistic == 'bias':
        #    subaxes.set_ylim(-0.5,0.7)
        #elif statistic == 'rank':
        #    subaxes.set_ylim(0,1)
        #    print('I set annual y axis lims to: {} {}'.format(0,1))
        #else:
        #    subaxes.set_ylim(-1,1)
        subaxes.set_ylabel('{} {}'.format(variable, statistic))

    def add_model_changes_to_graph(self, subaxes, list_of_dates):
        significant_model_changes = [
                ('201502','GFS, EC & WRF new high freq bins'),
                ('201503', 'APS1 new high freq bins'),
                ('201603', 'APS1 changed to APS2 winds'),
                ('201607', 'GFS 4.18 sole operational model'), 
                ('201612', 'CMC & NGM new high freq bins and 4.18 version'),
                ('201701', 'All models using new bathymetry data'),
                ('201702', 'WRF SCS GFS using new bathymetry data')
                ]
        pieces_of_info = 1
        for info in significant_model_changes:
            info_date = datetime.strptime(info[0], '%Y%m')
            if info_date in list_of_dates:
                y_min = subaxes.get_ylim()[0]
                subaxes.annotate(info[1], 
                        xy=(mdates.date2num(info_date),y_min),
                        xytext=(mdates.date2num(info_date)+8, 
                            y_min + (0.02*(pieces_of_info+1))),
                        fontsize=9,
                        arrowprops=dict(facecolor=None,edgecolor='black',
                            shrink=0.03, alpha=0.3)
                        )
                pieces_of_info = pieces_of_info + 1



    def mask_three_arrays(self, time_unmasked, obs_unmasked, model_unmasked):
        obs_mask = ~np.isnan(obs_unmasked)
        model_mask =~np.isnan(model_unmasked)
        full_mask = obs_mask * model_mask 
        obs_array = obs_unmasked[full_mask]
        model_array = model_unmasked[full_mask]
        time_array = time_unmasked[full_mask]
        return time_array, obs_array, model_array
    
    def get_model_attributes(self, model):
        if model.count('rain'):
            model = 'GFS'
        model_color = linestyles.line_styles[model].color
        model_linestyle = linestyles.line_styles[model].style    
        return model_color, model_linestyle

    def ts_routine_one(self, variable, vp, rain_var=False):
        """ Timeseries plotting routine for model verifications. """
        fig = None
        top_models = list(self.model_dictionary.keys())
        models = []
        for i in top_models:
            if vp in self.model_dictionary[i]:
                if rain_var == True:
                    if not i.count('ens'):
                        models.append(i)
                else:
                    models.append(i)
        n_plots = len(models)
        nrows = self.determine_no_rows(n_plots)
        y_max, y_min = self.get_largest_y_axis_value(n_plots, models, vp, 
                variable)
        actual_n = 0
        for n in range(0, n_plots):
            mod = models[n]
            time_var = getattr(self.observations_instance, 
                    '{}_array'.format('time'))
            model_var = getattr(self.model_dictionary[mod][vp],
                    '{}_array'.format(variable))
            obs_var = getattr(self.observations_instance, 
                    '{}_array'.format(variable))
            if rain_var:
                time_var, model_var = self.reaccumulate_rain_data(models, mod, vp)
                obs_var = self.reaccumulate_obs_rain_data(models)
            model_color, model_linestyle = self.get_model_attributes(mod)
            time_array, obs_array, model_array = self.mask_three_arrays(
                    time_var, obs_var, model_var)
            if len(time_array) > 0:
                if not fig:
                    fig = plt.figure(figsize=(11.69, 8.27))
                subaxes = fig.add_subplot(nrows, 2, actual_n+1)
                if self.location == 'Cuulong' or (self.veri_type =='global_winds' and self.location == 'Ichthys'):
                    subaxes.plot(time_array, obs_array, color='k', label='Obs data')
                    subaxes.plot(time_array, model_array, color=model_color, 
                            linestyle=model_linestyle, label=mod)
                    self.set_timeseries_x_ticks(subaxes, time_array, hour_inc=6)
                else:
                    subaxes.plot(time_var, obs_var, color='k', label='Obs data')
                    subaxes.plot(time_var, model_var, color=model_color, 
                            linestyle=model_linestyle, label=mod)
                    self.set_timeseries_x_ticks(subaxes, time_var)
                self.set_timeseries_y_ticks(subaxes, variable, y_max, y_min)
                if n == n_plots-1 or n == n_plots-2:
                    subaxes.set_xlabel('Date (utc)', fontsize=8)
                if n%2 == 0:
                    subaxes.set_ylabel('{}'.format(variable), fontsize=8)
                subaxes.grid()
                subaxes.legend(fontsize=8)
                actual_n += 1
        if fig:
            fig.suptitle('{} timeseries plot for {}(tau{})'.format(self.location, 
                variable, vp))
            plt.tight_layout()
            plt.subplots_adjust(top=0.95)
        return fig
   

    def reaccumulate_rain_data(self, models, model, vp):
        if 'GFS' in models:
            acc_hr = 3
        else:
            acc_hr = 1
        dates = []
        the_start = self.start_date
        the_end = self.end_date
        while the_start<the_end:
            dates.append(the_start)
            the_start = the_start +timedelta(hours=acc_hr)
        variable = 'rain'
        var_list = []
        dates.sort()
        try:
            model_var = getattr(self.model_dictionary[model][vp],
                    '{}_array'.format(variable))
        except:
            model_var = []
        orig_time_array = self.model_dictionary[model][vp].time_array
        for index, val in enumerate(dates):
            dt = val
            #print(dt)
            if index == 0:
                acc_rain = np.nan
                prev_dt = dt
            else:
                prev_dt = dates[index-1]
            acc_rain = [] 
            for raw_dt in orig_time_array:
                if raw_dt <= dt and raw_dt > prev_dt:
                    time_list = list(orig_time_array)
                    rain_index = time_list.index(raw_dt)
                    rain_val = model_var[rain_index]
                    acc_rain.append(rain_val)
            #print('What found: {}'.format(acc_rain))
            acc_init = np.array(acc_rain)
            mask = np.isfinite(acc_init)
            acc_array = acc_init[mask]
            #print('What left: {}'.format(acc_array))
            if len(acc_array):
                acc_val = 0
                for i in acc_array:
                    acc_val = acc_val + i
            else:
                acc_val = np.nan
            var_list.append(acc_val)
            #print('What added: {}'.format(acc_val))
        #import ipdb;ipdb.set_trace()
        return np.array(dates), np.array(var_list)

    def reaccumulate_obs_rain_data(self, models):
        variable = 'rain'
        if 'GFS' in models:
            acc_hr = 3
        else:
            acc_hr = 1
        dates = []
        the_start = self.start_date
        the_end = self.end_date
        while the_start<the_end:
            dates.append(the_start)
            the_start = the_start +timedelta(hours=acc_hr)
        new_list = []
        dates.sort()
        obs_var = self.observations_instance.rain_array
        orig_time_array = self.observations_instance.time_array
        for index, val in enumerate(dates):
            dt = val
            obs_vals  = []
            if index == 0:
                acc_rain = np.nan
                prev_dt = dt
            else:
                prev_dt = dates[index-1]
            acc_rain = 0 
            for raw_dt in orig_time_array:
                if raw_dt <= dt and raw_dt > prev_dt:
                    time_list = list(orig_time_array)
                    rain_index = time_list.index(raw_dt)
                    rain_val = obs_var[rain_index]
                    obs_vals.append(rain_val)
            acc_init = np.array(obs_vals)
            mask = np.isfinite(acc_init)
            acc_array = acc_init[mask]
            if len(acc_array):
                acc_val = 0
                for i in acc_array:
                    acc_val = acc_val + i
            else:
                acc_val = np.nan
            new_list.append(acc_val)
        return np.array(new_list)






    def get_largest_y_axis_value(self, n_plots, models, vp, variable):
        high_vals = []
        for n in range(0, n_plots):
            mod = models[n]
            if variable == 'rain':
                if mod.count('ens'):
                    continue
            model_array = getattr(self.model_dictionary[mod][vp],
                    '{}_array'.format(variable))
            obs_array = getattr(self.observations_instance, 
                    '{}_array'.format(variable))
            model_max = np.nanmax(model_array)
            model_min = np.nanmin(model_array)
            ob_max = np.nanmax(obs_array)
            ob_min = np.nanmin(obs_array)
            high_vals.append(model_max)
            high_vals.append(model_min)
            high_vals.append(ob_max)
            high_vals.append(ob_min)
        y_max = np.nanmax(high_vals)
        y_min = np.nanmin(high_vals)
        return y_max, y_min

    def set_timeseries_x_ticks(self, subaxes, time_array, hour_inc=0, day_inc=3):
        xticks = []
        xlabs = []
        for i in time_array:
            if i.hour == hour_inc:
                if i.day %day_inc == 0:
                    xticks.append(mdates.date2num(i))
                    xlabs.append(i.strftime('%d\n%b'))
        first_xtick = mdates.date2num(time_array[0])
        last_xtick = mdates.date2num(time_array[-1])
        subaxes.set_xlim(first_xtick, last_xtick)
        subaxes.xaxis.set_ticks(xticks)
        subaxes.set_xticklabels(xlabs, fontsize=8)
        
    def set_timeseries_y_ticks(self, subaxes, variable, y_max, y_min, label_incr=5):
        yticks = []
        ylabs = []
        if variable == 'dr' or variable == 'wdir':
            yticks = [0,90,180,270,360,450,540]
            ylabs = [0,90,180,270,360,450,540]
        else:
            #y_min = (subaxes.get_ylim()[0])
            if y_min <= 0:
                y_min = 0
            tick_inc = y_max/label_incr
            yticks = np.arange(y_min, y_max+tick_inc, tick_inc)
            for i in yticks:
                ylabs.append(round(i,1))
        subaxes.set_yticks(yticks)
        subaxes.set_yticklabels(ylabs, fontsize=8)
    
    def mask_two_arrays(self, obs_unmasked, model_unmasked):
        obs_mask = ~np.isnan(obs_unmasked)
        model_mask =~np.isnan(model_unmasked)
        full_mask = obs_mask * model_mask 
        obs_array = obs_unmasked[full_mask]
        model_array = model_unmasked[full_mask]
        return obs_array, model_array

    def er_routine_one(self, variable, vp):
        """ Error plot plotting routine for model verifications."""
        fig = None
        top_models = list(self.model_dictionary.keys())
        models = []
        for i in top_models:
            if vp in self.model_dictionary[i]:
                models.append(i)
        n_plots = len(models)
        nrows = self.determine_no_rows(n_plots)
        for n in range(0, n_plots):
            mod = models[n]
            try:
                model_var = getattr(self.model_dictionary[mod][vp],
                        '{}_array'.format(variable))
            except:
                print('couldnt get model {} for error plot'.format(mod))
            obs_var = getattr(self.observations_instance, 
                    '{}_array'.format(variable))
            obs_array, model_array = self.mask_two_arrays(obs_var, model_var)
            model_color, model_linestyle = self.get_model_attributes(mod)
            #if variable == 'tp':
            #    import ipdb;ipdb.set_trace()
            if len(model_array) >0 :
                diff = (model_array - obs_array)
                
                #if variable == 'tp':
                #    import ipdb;ipdb.set_trace()
                step_size, r_min, r_max = self.errorplot_attributes(variable)
                bin_range = np.arange(r_min, r_max, step_size) 
                count, values = np.histogram(diff, bins=bin_range)
                count_percents = (count/len(diff))*100
                if not fig:
                    fig = plt.figure(figsize=(11.69, 8.27))
                subaxes = fig.add_subplot(nrows, 2, n+1)
                subaxes.bar(values[:-1], count_percents, step_size, align='edge',
                        label=mod, color=model_color, edgecolor='k')
                self.set_errorbar_x_ticks(subaxes, r_min, r_max, step_size)
                try:
                    self.set_errorbar_y_ticks(subaxes)
                except:
                    print('Couldnt set yticks on errorplot for {} in {}({})'.format(mod, variable, vp))
                    import ipdb;ipdb.set_trace()
                if n == n_plots-1 or n == n_plots-2:
                    subaxes.set_xlabel('Bias', fontsize=8)
                if n%2 == 0:
                    subaxes.set_ylabel('Percentage of bias (%)', fontsize=8)
                subaxes.grid()
                subaxes.legend(fontsize=8)
        return fig
    
    def set_errorbar_x_ticks(self, subaxes, r_min, r_max, step_size):
        x_min = r_min
        x_max = r_max
        my_xticks = list(np.arange(x_min, x_max+step_size, step_size*2))
        my_xlabs = []
        my_count = 0
        for i in my_xticks:
            tick = round(i, 1)
            if (my_count%2) ==0:
                my_xlabs.append(tick)
            else:
                my_xlabs.append(' ')
            my_count += 1
        subaxes.set_xticks(my_xticks)
        subaxes.set_xticklabels(my_xlabs, fontsize=8)
        #subaxes.set_xlabel('bias', fontsize=8)

    def set_errorbar_y_ticks(self, subaxes, label_incr=5):
        y_min = int((subaxes.get_ylim()[0]))
        y_max = int(subaxes.get_ylim()[1])
        if y_max >= 100:
            y_max = 100.
        yticks = []
        ylabs = []
        tick_inc = y_max/label_incr
        yticks = np.arange(y_min, y_max+tick_inc, tick_inc)
        for i in yticks:
            ylabs.append(round(i,1))
        subaxes.set_yticks(yticks)
        subaxes.set_yticklabels(ylabs, fontsize=8)
        #subaxes.set_ylabel('Percentage of bias (%)', fontsize=8)

    def errorplot_attributes(self, variable):
        """ Set histogram details. """
        if variable == 'hs':
            step_size = 0.2
            bin_min = -2
            bin_max = 2
        if variable == 'tp':
            step_size = 0.5
            bin_min = -15
            bin_max = 15 
        if variable == 'dr' or variable == 'wdir':
            step_size = 24.0
            bin_min = -180
            bin_max = 180
        if variable == 'wspd':
            step_size = 4
            bin_min = -20
            bin_max = 20
        if variable == 'temp':
            step_size = 4
            bin_min = -10
            bin_max = 20
        if variable == 'rain':
            step_size = 0.2
            bin_min = -10
            bin_max = 10
        return step_size, bin_min, bin_max

    def sc_routine_one(self, variable, vp):
        """ Scatter plot plotting routine for model verifications."""
        fig = None
        top_models = list(self.model_dictionary.keys())
        models = []
        for i in top_models:
            if vp in self.model_dictionary[i]:
                models.append(i)
        n_plots = len(models)
        ncols = self.determine_no_rows(n_plots)
        nrows = 2
        for n in range(0, n_plots):
            mod = models[n]
            try:
                model_var = getattr(self.model_dictionary[mod][vp],
                        '{}_array'.format(variable))
            except:
                print('coulndt get model {} for scatterplot'.format(mod))
            obs_var = getattr(self.observations_instance, 
                    '{}_array'.format(variable))
            obs_array, model_array = self.mask_two_arrays(obs_var, model_var)
            model_color, model_linestyle = self.get_model_attributes(mod)
            if len(model_array) > 0:
                max_value = self.find_max_value(obs_array, model_array)
                range_x = np.arange(0, max_value+0.1, 0.1)
                rr, gradient, equation = self.determine_sc_attributes(obs_array, 
                        model_array, range_x)
                if not fig:
                    fig = plt.figure(figsize=(11.69, 8.27))
                subaxes = fig.add_subplot(nrows, ncols, n+1)
                subaxes.scatter(obs_array, model_array, color=model_color,
                        linestyle=model_linestyle, edgecolors='k', label=mod)
                subaxes.plot(range_x, range_x, color='k', label='Perfect')
                subaxes.plot(range_x, gradient, color=model_color, 
                        label='Line of best fit')
                self.set_scatterplot_xy_ticks(subaxes, variable)
                subaxes.text(0.7, 0.2, 'rr: {}'.format(rr), fontsize=8, 
                        transform=subaxes.transAxes)
                subaxes.text(0.7, 0.1, equation, fontsize=8, 
                        transform=subaxes.transAxes)
                subaxes.grid()
                subaxes.legend(fontsize=8, loc='upper left')
                if n > 3:
                    subaxes.set_xlabel('Observations', fontsize=8)
                if n == 0 or n == 4:
                    subaxes.set_ylabel('Model', fontsize=8)
        if fig:
            plt.tight_layout()
        return fig

    def determine_sc_attributes(self, obs_array, model_array, range_x):
        slope, intercept, r_value, p_value, std_err = (
                scipy.stats.linregress(obs_array, model_array))
        rr = round(r_value * r_value, 2)
        gradient = (slope * range_x) + intercept  
        slope = round(slope, 2)
        intercept = round(intercept, 2)
        if intercept >= 0:
            int_sign = '+'
        else:
            int_sign = ''
        equation_str = 'f(x)={}x{}{}'.format(slope, int_sign, intercept)
        return rr, gradient, equation_str 

    def find_max_value(self, obs_array, model_array):
        obs_max = np.nanmax(obs_array)
        model_max = np.nanmax(model_array)
        if obs_max >= model_max:
            return obs_max
        else:
            return model_max

    def set_scatterplot_xy_ticks(self, subaxes, variable, label_incr=7):
        if variable == 'dr' or variable== 'wdir':
            axis_ticks = [0,90,180,270,360,450,540]
            axis_labels = [0,90,180,270,360,450,540]
        else:
            x_min = subaxes.get_xlim()[0]
            x_max = subaxes.get_xlim()[1]
            y_min = subaxes.get_ylim()[0]
            y_max = subaxes.get_ylim()[1]
            if x_min <= 0:
                the_min = 0
            else:
                if x_min <= y_min:
                    the_min = x_min 
                else:
                    the_min = y_min
            if x_max >= y_max:
                the_max = y_max
            else:
                the_max = x_max
            subaxes.set_xlim(the_min, the_max)
            subaxes.set_ylim(the_min, the_max)
            axis_ticks = []
            axis_labels = []
            tick_inc = the_max/label_incr
            axis_ticks = np.arange(the_min, the_max+tick_inc, tick_inc)
            for i in axis_ticks:
                axis_labels.append(round(i,1))
        subaxes.xaxis.set_ticks(axis_ticks)
        subaxes.set_xticklabels(axis_labels, fontsize=8)
        subaxes.yaxis.set_ticks(axis_ticks)
        subaxes.set_yticklabels(axis_labels, fontsize=8)



