""" This verification module is used as the driver for monthly verification 
proceedures. """
#NOTE - Stef has found a massive bug on November 20th 2017....the 
#obs_path has the start_date.year at the end. This means the scripts cannot 
#be used to produce annual stats at the same time... Bummer will need to either
#put in a year loop and need to consider every time obs_path is used in ALL 
#scripts. 
#Potential bug fix in retrieval of previous summary stats files where a
#path.split fiunction has beeen used in some functions. 5/12/17- Stef

import os

import calendar
import numpy as np
from datetime import datetime
from dateutil.relativedelta import relativedelta

import setup_functions as sf
import ows_pyobs as oc
import ows_pymods as mc
import statistics_class as sc
import plotting_class as pc
import postprocessing_functions as pf

class Verification():

    """
    Verification Class for model verifications.

    """

    def __init__(self, start_date=None, end_date=None, verification_types=[],
            base_dir='Dev', custom_use=False):
        """ 
        """
        if base_dir =='Dev':
            print('NOTE!!!! #### Results going to Dev dir.\n \n')
            self.base_dir = os.path.join('/home/Operator', 'Development')
        elif base_dir == 'Op':
            self.base_dir = os.path.join('/home/Operator', 'Operational')
        else:
            print('NOTE!!!! #### Results going to Dev dir.\n \n')
            self.base_dir = os.path.join('/home/Operator', 'Development')
        
        self.custom_use = custom_use
        if not start_date:
            self.determine_start_and_end_date()
        else:
            self.start_date = start_date
            self.end_date = end_date
        print(self.start_date)
        print(self.end_date)
        self.determine_locations() 
        if verification_types:
            self.verification_types = verification_types
        else:
            self.verification_types = [
                    'wave_watch_3',
                    'global_winds',
                    'wrf',
                    'wave_watch_3_outlook_5_days',
                    'wave_watch_3_outlook_10_days'
                    ]

    def run_a_verification(self):
        """ Using the instantiated attributes, run a model verification for
        each type of verification and each location. """
        for veri_type in self.verification_types:
            print(self.verification_types)
            print('Doing verification/s for {} model/s'.format(veri_type))
            self.veri_type = veri_type
            for location in self.locations:
                self.location = location
                print('Currently doing veri for {}'.format(self.location))
                self.determine_verification_parameters()
                # create observations instance
                observations_instance = oc.Observations(self.start_date, 
                        self.end_date, 
                        self.location, 
                        self.obs_path, 
                        verification_type=self.veri_type)
                
                # call method to get observations data and process it 
                # accordingly 
                if self.veri_type.count('wave_watch_3'):
                    obs_exist = observations_instance.verification_method1()
                else:
                    obs_exist = observations_instance.verification_method2()
                if obs_exist:
                    # create a statistics instance 
                    statistics_instance = sc.Statistics(self.start_date, 
                            self.end_date, 
                            self.location, 
                            observations_instance, 
                            self.obs_path,  
                            verification_type=self.veri_type, 
                            variables=self.variables, 
                            models=self.models)
                    # calculate the observations statistics
                    statistics_instance.calculate_and_write_observations_statistics()
                    # create model instance
                    model_instance = mc.Models(self.start_date, 
                            self.end_date,  
                            self.location, 
                            self.model_path, 
                            self.models, 
                            save_path=self.obs_path, 
                            verification_type=self.veri_type, 
                            verification_periods=self.vperiods, 
                            observations_instance=observations_instance)
                    # call method to get model data and process it accordingly
                    if self.veri_type.count('wave_watch_3'):
                        model_dictionary, models_exist = \
                                model_instance.verification_method1()
                    else:
                        model_dictionary, models_exist = \
                                model_instance.verification_method2()
                    if models_exist:
                        # create a second statistics instance
                        statistics_instance2 = sc.Statistics(self.start_date, 
                                self.end_date, 
                                self.location, 
                                observations_instance, 
                                self.obs_path, 
                                model_dictionary=model_dictionary, 
                                rank_period=self.rank, 
                                verification_type=self.veri_type, 
                                variables=self.variables, 
                                models=self.models, 
                                verification_periods=self.vperiods)
                        # calculate the summary statistics for obs vs model data
                        if self.veri_type == 'wrf':
                            statistics_instance2.calculate_and_write_wrf_summary_statistics()
                        else:
                            statistics_instance2.calculate_and_write_summary_statistics()
                            if self.base_dir.count('Operational'):
                                statistics_instance2.calculate_and_write_bias_stats() 
                        # create a plotting instance 
                        plot_instance = pc.Plotting(observations_instance, 
                                location=self.location, 
                                save_path=self.obs_path,
                                start_date=self.start_date, 
                                end_date=self.end_date, 
                                model_dictionary=model_dictionary, 
                                verification_periods=self.vperiods, 
                                variables=self.variables, 
                                verification_type=self.veri_type, 
                                models=self.models)
                        # call method to plot the timeseries, scatterplot and 
                        # errorplot and annual plots 
                        if self.veri_type =='wrf':
                            plot_instance.verification_method2()
                        else:
                            plot_instance.verification_method1()
                        # call methods to copy results from virtual machine to
                        # the archive drive, plots then files. 
                        
                        if self.base_dir.count('Operational'):
                            print('Now copying all files needed to Archive drive...')
                            pf.annual_plots_vm_to_archive(self.obs_path, 
                                    self.variables, 
                                    self.veri_type, 
                                    self.location)
                            pf.files_from_vm_to_archive(self.start_date, 
                                    self.obs_path, 
                                    self.variables, 
                                    self.veri_type, 
                                    self.location)
                    else:
                        print('There was no model data for this location, so'
                                + ' ending this verification attempt..\n')
                else:
                    print('The observations data didnt exist, so ending '
                            + 'verification attempt for this location...\n')
                    continue
                print('Finished verification for this location!\n')
            # call method which emails results to stefs email. 
            if self.base_dir.count('Operational'):
                statistics_instance2.calculate_and_write_bias_stats() 
                pf.email_results_to_stef(self.start_date, 
                        self.veri_type, 
                        self.locations, 
                        self.obs_path, 
                        self.vperiods, 
                        self.variables)

    def determine_start_and_end_date(self):
        """ Using datetime.now() determine the previous month for which the
        verification will be completed for. """
        now_datetime = datetime.now()
        start_month = now_datetime - relativedelta(months=1)
        start_date = datetime(start_month.year, start_month.month, 1, 0, 0)
        end_date = datetime(start_date.year, start_date.month,
                calendar.monthrange(start_date.year, start_date.month)[1], 23, 0)
        self.start_date = start_date
        self.end_date = end_date

    @property
    def locations(self):
        """ Retrieve a list of all the locations to be verified, despite the 
        type of verification. """
        current_dir = os.path.dirname(os.path.realpath(__file__))
        if self.custom_use:
            loc_file = os.path.join(current_dir, 'custom_locations.txt')
        else:
            loc_file = os.path.join(current_dir, 'locations.txt')
        
        locations = []
        #if os.path.isfile('locations.txt'):
        if os.path.isfile(loc_file):
            file_exists = True
        else:
            file_exists = False
        if file_exists:
            #lid = open('locations.txt', 'r')
            lid = open(loc_file, 'r')
            with lid:
                for line in lid:
                    the_line = line.strip('\n')
                    if the_line == '':
                        continue
                    locations.append(the_line)
            if locations:
                return locations
            else:
                print('There was no list of locations to complete this '
                        + 'verification.. Quitting.')
                return
        else:
            print('The file with locations in it doesnt exist. '
                        + ' Quitting.')
            return

    def determine_locations(self):
        try:
            self.locations
        except TypeError:
            return

    def determine_verification_parameters(self):
        """ Depending on the type of verification, assign the attributes that 
        will be needed for the verification to be completed. """
        if self.veri_type == 'wave_watch_3':
            import wave_watch_3_information as vpid
            self.model_path = os.path.join('/mnt/ows/Archive1/Models', 
                    'WW3_output')
            sf.move_wave_model_output_to_dirs(self.start_date, self.model_path)
        elif self.veri_type == 'wave_watch_3_outlook_5_days':
            import wave_watch_3_outlook_5_days_information as vpid
            self.model_path = os.path.join('/mnt/ows/Archive1/Models', 
                    'WW3_output')
            sf.move_wave_model_output_to_dirs(self.start_date, self.model_path)
        elif self.veri_type == 'wave_watch_3_outlook_10_days':
            import wave_watch_3_outlook_10_days_information as vpid
            self.model_path = os.path.join('/mnt/ows/Archive1/Models', 
                    'WW3_output')
            sf.move_wave_model_output_to_dirs(self.start_date, self.model_path)
        elif self.veri_type == 'global_winds':
            import global_winds_information as vpid
            self.model_path = os.path.join('/mnt/ows/Archive1/Models', 
                    'WW3_output')
            sf.move_wave_model_output_to_dirs(self.start_date, self.model_path)
        elif self.veri_type == 'wrf':
            import wrf_information as vpid
            self.model_path = os.path.join('/mnt/ows/Archive1/Models', 
                    'WRF')
            sf.move_weather_model_output_to_dirs(self.start_date, self.model_path)
        
        self.models = vpid.Models
        self.variables = vpid.Variables
        self.vperiods = np.arange(vpid.Start_vp, vpid.End_vp, vpid.Period)
        self.rank = vpid.Rank
        self.obs_path = os.path.join(self.base_dir,
                'model_verifications', 'results', self.veri_type, 
                self.location, str(self.start_date.year))

if __name__ == '__main__':
   verification_instance = Verification(base_dir='Dev')
   verification_instance.run_a_verification()

