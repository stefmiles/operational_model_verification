#!/usr/bin/env python
import matplotlib
matplotlib.use('Agg')

import os

import numpy as np
from collections import defaultdict
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from argparse import ArgumentParser

import owsdb_ssh
from owssendemail import send_mail

def check_new_locations(Locations):
    """ Records the locations which have been added to the 
    model_forecasts_points.txt and returns them in a list. """
    mveri_file = os.path.join('/mnt/fs02/fs02/forecast_points', 
            'model_verification_input.txt')
    mid = open(mveri_file, 'r')
    input_locations = []
    with mid:
        for line in mid:
            try:
                loc = line.split()[2]
                lat = line.split()[1]
                lat_fl = float(lat)
                lon = line.split()[0]
                if lat_fl < 0:
                    mp = '{}S{}E'.format(lat, lon)
                else:
                    mp = '{}N{}E'.format(lat, lon)
            except:
                continue
            input_locations.append((loc, mp))
    missing_locs = []
    for in_loc in input_locations:
        if in_loc[0] not in Locations:
            missing_locs.append(in_loc)
    return missing_locs

def send_email(missing_locs):
    """ Sends email to Stef if there has been a location added to the 
    model_verification_input.txt. """
    to_list = ['stephanie@offshoreweather.com.au', 'simon@offshoreweather.com.au']
    subject = 'New location added to model verifications'
    message = ('There has been new locations added to the model verification'
            + ' list. These are {}'.format(missing_locs)
            + '\nYou\'ll need to please work out where the obs are now..')
    send_mail(to_list, subject, message)

def main():
    """ Checks to see whether a new location has been added to the
    model_verification_input.txt file or not and adds them to the 
    locations.txt file and sends an email if so. """
    Locations = []
    lid = open('locations.txt','r')
    with lid:
        for line in lid:
            the_line = line.strip('\n')
            if the_line == '':
                continue
            Locations.append(the_line)
    missing_locs = check_new_locations(Locations)
    if missing_locs:
        lid = open('locations.txt','a')
        with lid:
            for loc in missing_locs:
                lid.write(loc[0] + '\n')
        send_email(missing_locs)

def get_site_id(location):
    """ Find the site_id for your location on the database."""
    with owsdb_ssh.Connection() as conn:
        cur = conn.cursor()
 
        cur.execute("SELECT site_id, site_name "
                    "FROM weather.wx_sites;")
        sources = cur.fetchall()
        matches = []
        for item in sources:
            loc_str = item[1]
            loc_split = item[1].split()
                
            if any(x in location for x in loc_split):
                matches.append(item)
        cur.close()
    return matches


def get_variables(location_matches):
    '''Print all variable_id, source_id pairs associated with a site.'''
    
    for location in location_matches:
        print('THIS IS THE LOCATION: {} WITH SITE_ID: {}'.format(location[1], location[0]))
        site_id = location[0]
        with owsdb_ssh.Connection() as conn:
            cur = conn.cursor()
            # Find all sources associated with the site
            cur.execute("SELECT source_id, source_name, source_type "
                        "FROM weather.wx_sources "
                        "WHERE site_id=%s;",
                        [site_id])
            sources = cur.fetchall()
            # Read the main obs table and find every disinct variable_id associated
            # with the source_ids we just found.
            count = 0
            source_variable_pairs = []
            for source in sources:
                cur.execute("SELECT DISTINCT variable_id, source_id "
                            "FROM weather.wx_obs_data "
                            "WHERE source_id=%s;", [source[0]])
                source_variable_pairs.extend(cur.fetchall())
                count += 1
                print('main db read {0}% done'.format(100 * float(count) / len(sources)))

            # Get the whole varibles table, so we have the names associated with
            # the variable ids.
            cur.execute("SELECT variable_id, variable_name, sample_duration "
                        "FROM weather.wx_variables ")
            variables = cur.fetchall()
            cur.close()
        sources_dict = {x[0]: x[1:] for x in sources}
        variables_dict = {x[0]: x[1:] for x in variables}
        row_str = '{0:>15} {1:>20} {2:>20} {3:>15} {4:>15} {5:>20}'
        print(row_str.format('variable_id', 'variable_name', 'sample_duration',
                             'source_id', 'source_name', 'source_type'))
        print('--------------------------------------------------------'
              '-------------------------------------------------------')
        source_variable_pairs.sort()
        for pair in source_variable_pairs:
            var_id = pair[0]
            var_name = variables_dict[pair[0]][0]
            sample_duration = variables_dict[pair[0]][1] or ''
            source_id = pair[1]
            source_name = sources_dict[pair[1]][0]
            source_type = sources_dict[pair[1]][1]
            print(row_str.format(var_id, var_name, sample_duration,
                                 source_id, source_name, source_type))
        print('\n')


def check_if_data_on_database(variable_id, source_id, year, month):
    """ Checks if observations data with specific variable and source id is 
    avaialble on the data base. Returns in a dictionary if it is. """ 
    rows = defaultdict(dict)
    with owsdb_ssh.Connection() as conn:
        cur = conn.cursor() 
        cur.execute("SET TimeZone = 'UTC'; "
                    "SELECT obs_time, obs_quantity "
                    "FROM weather.wx_obs_data "
                    "WHERE variable_id=%s "
                    "AND source_id=%s "
                    "AND date_part('year', obs_time)=%s "
                    "AND date_part('month', obs_time)=%s; ",
                    (variable_id, source_id, year, month))
        results = cur.fetchall()
        if not results:
            print('No data on the database for this variable.')
        else:
            print('There was data on the database for this variable.')
            for result in results:
                rows[result[0]]['datetime'] = result[0]
                rows[result[0]] = result[1]
    return rows 

def plot_database_data(dates, variable_array):
    """ Plots a timeseries plot of database data. """
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(dates, variable)
    xlabs = []
    xticks = []
    for i in dates:
        if i.hour == 0:
            if i.day %3 == 0:
                xticks.append(mdates.date2num(i))
                xlabs.append(i.strftime('%d\n%b'))
    if xticks:
        first_xtick = mdates.date2num(dates[0])
        last_xtick = mdates.date2num(dates[-1])
        ax.set_xlim(first_xtick, last_xtick)
        ax.xaxis.set_ticks(xticks)
    if xlabs:
        ax.set_xticklabels(xlabs, fontsize=8)
        ax.set_xlabel('date', fontsize=8)
    else:
        print('Couldnt determine x axis labels....')
    print('A plot of this data has been made and is saved in this current location called "test.png"')
    plt.savefig('test.png')
    plt.close(fig)


def float_date(line, column):
    try: 
        my_data = line[column]
    except:
        my_data = np.nan
    try: 
        float_data = round(float(my_data),2)
    except:
        float_data = np.nan
    return float_data


def mv_and_format_obs_file(start_date, mv_to_path):
    """ This is a general function which can be called to create observations
    files which are going to be used for verifications.. into the correct 
    working directories in in the same format as if they came off the database. 
    PLEASE FILL OUT THE NECESSARY INFORMATION.
    """
    #### PLEASE DETERMINE THESE:
    date_column = 0
    date_format = '%Y%m%d%H%M'
    var1_column = 1
    var2_column = 2
    var3_column = 3
    path_of_obs = os.path.join('/mnt/server1/server1')
    ####

    yearmonth = str(start_date.strftime('%Y%m'))
    save_path = os.path.join(mv_path, 'obs_csv_files', yearmonth + '.csv')
    sid = open(sid, 'w')
    oid = open(path_of_obs, 'r')
    with oid:
        with sid:
            for line in oid:
                line_split = line.split('ENTER LINE SPLIT NOTATION')
                # add any skip lines in here if needed
                dt = datetime.strptime(line_split[date_column], date_format)
                str_dt = dt.strftime('%Y-%m-%d %H:%M:%S+00:00')
                variable1 = float_data(line_split, var1_column)
                variable2 = float_data(line_split, var2_column)
                variable3 = float_data(line_split, var3_column)
                line2write = '{},{},{},{},\n'.format(str_dt, variable1, 
                        variable2, variable3)
                sid.write(line2write)

def check_where_model_point(model_point):
    """ This function is to help me set up the forecast paths in the model
    class. Specifically, it can be helpful in determining the basin the point
    lies within. """
    found = False
    mp_lon = float(model_point[0])
    mp_lat = float(model_point[1])
    list_of_files = os.listdir(os.path.join('/mnt/fs02/fs02',
        'model','Ocean','WW3','IN','POINTS'))
    for the_file in list_of_files:
        if the_file.count('swp'):
            continue
        if the_file.count('Shell'):
            file_path = os.path.join('/mnt/fs02/fs02',
                    'model','Ocean','WW3','IN','POINTS', the_file)
            try:
                fid = open(file_path, encoding='iso-8859-1')
            except:
                fid = open(file_path,'r')
            with fid:
                for line in fid:
                    lon = float(line.split()[0])
                    lat = float(line.split()[1])
                    if lon == mp_lon:
                        if lat == mp_lat:
                            found = True
                            path_split1 = os.path.split(file_path) 
                            up_path = path_split1[1]
                            print('This model point was found in {}'.format(up_path))
    if not found:
        print('This model point was not found in the WW3 IN POINTS')


def move_weather_model_output_to_dirs(start_date, model_path):
    print('Making new directories for forecast files..(Can take minutes)')
    clean_list_arw = ['BASS_ARW_Operational', 'BRN_ARW_Operational', 
            'GoT_ARW_Operational', 'NWS_ARW_Operational']
    clean_list_nmm = ['BOB_NMM_Operational', 'GoT_NMM_Operational', 
            'NWS_NMM_Operational', 'SCS_NMM_Operational', 
            'TIMOR_NMM_Operational']
    clean_list = clean_list_arw + clean_list_nmm
    year = str(start_date.year)
    for folder_name in clean_list:
        my_path = os.path.join(model_path, folder_name, year)
        if not os.path.isdir(my_path):
            os_command = 'mkdir ' + my_path
            os.system(os_command)
        for month in ['01', '02', '03', '04', '05', '06', '07', '08', '09',
                '10', '11', '12']:
            my_path = os.path.join(model_path, folder_name, year, year+month)
            if not os.path.isdir(my_path):
                os_command = 'mkdir ' + my_path
                os.system(os_command)
            for folder in os.listdir(os.path.join(model_path, folder_name)):
                if year+month in folder:
                    os_command = ('mv ' 
                            + os.path.join(model_path, folder_name, folder) 
                            + ' ' 
                            + os.path.join(model_path, folder_name, year, year+month)
                            + ' > /dev/null 2>&1')
                    os.system(os_command)



def move_wave_model_output_to_dirs(start_date, model_path):
    """ Moves all the model forecasts into respective month and yearly 
    folders rather than the structure Simons output is organised in."""
    print('Making new directories for forecast files.. (Can take minutes)')
    clean_list_globals = ['GFS418_Global','GFS_Global','GFST2_Global',
            'NGM_Global','APS_Global','CMC_Global','GFX_Global', 'GSM_Global']
    clean_list_gfs418 = ['BASSX_GFS418_Nested','BIGHT_GFS418_Nested',
            'CHINA_GFS418_Nested','JAVA_GFS418_Nested','SCS_GFS418_Nested',
            'TIMOR_GFS418_Nested','WA_GFS418_Nested','EC_GFS418_Nested']
    clean_list_gfs = ['BASSX_GFS_Nested','BIGHT_GFS_Nested','CHINA_GFS_Nested',
            'JAVA_GFS_Nested','SCS_GFS_Nested','TIMOR_GFS_Nested',
            'WA_GFS_Nested']
    clean_list_gfst2 = ['BASSX_GFST2_Nested','BIGHT_GFST2_Nested',
            'TIMOR_GFST2_Nested','WA_GFST2_Nested']
    clean_list_gfx = ['BENGAL_GFX_Nested']
    clean_list_ngm = ['BASSX_NGM_Nested','BENGAL_NGM_Nested',
            'BIGHT_NGM_Nested','CHINA_NGM_Nested','JAVA_NGM_Nested',
            'SCS_NGM_Nested','TIMOR_NGM_Nested','WA_NGM_Nested']
    clean_list_ec = ['BASSX_EC_Nested','BENGAL_EC_Nested','BIGHT_EC_Nested',
            'CHINA_EC_Nested','JAVA_EC_Nested','SCS_EC_Nested',
            'TIMOR_EC_Nested','WA_EC_Nested']
    clean_list_aps = ['BASSX_APS_Nested','BENGAL_APS_Nested',
            'BIGHT_APS_Nested','CHINA_APS_Nested','JAVA_APS_Nested',
            'SCS_APS_Nested','TIMOR_APS_Nested','WA_APS_Nested']
    clean_list_cmc = ['BASSX_CMC_Nested','BENGAL_CMC_Nested',
            'BIGHT_CMC_Nested','CHINA_CMC_Nested','JAVA_CMC_Nested',
            'SCS_CMC_Nested','TIMOR_CMC_Nested','WA_CMC_Nested']
    clean_list_gsm = ['BASSX_GSM_Nested','BENGAL_GSM_Nested',
            'CHINA_GSM_Nested','JAVA_GSM_Nested',
            'SCS_GSM_Nested','TIMOR_GSM_Nested','WA_GSM_Nested']
    clean_list_wrf = ['WRF_NWS_WA418_Nested', 'WRF_SCS_GFS418_Nested',
            'WRF_THAI_SCS418_Nested','WRF_TIMOR_GFS418_Nested']
    clean_list = (clean_list_globals + clean_list_gfs418 
            + clean_list_gfs + clean_list_gfst2 
            + clean_list_ngm + clean_list_ec 
            + clean_list_aps + clean_list_cmc
            + clean_list_gsm 
            + clean_list_wrf + clean_list_gfx
            )
    year = str(start_date.year)
    for folder_name in clean_list:
        my_path = os.path.join(model_path, folder_name, year)
        if not os.path.isdir(my_path):
            os_command = 'mkdir ' + my_path
            os.system(os_command)
        for month in ['01', '02', '03', '04', '05', '06', '07', '08', '09',
                '10', '11', '12']:
            my_path = os.path.join(model_path, folder_name, year, year+month)
            if not os.path.isdir(my_path):
                os_command = 'mkdir ' + my_path
                os.system(os_command)
            for folder in os.listdir(os.path.join(model_path, folder_name)):
                if year+month in folder:
                    os_command = ('mv ' 
                            + os.path.join(model_path, folder_name, folder) 
                            + ' ' 
                            + os.path.join(model_path, folder_name, year, year+month)
                            + ' > /dev/null 2>&1')
                    os.system(os_command)









if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('-l', '--location',
            help='The location to check on the database',
            action='store', type=str, required=False)
    parser.add_argument('-mp', '--model_point',
            help='The model point you want to check, needs to be in the form of lon lat. eg. 151.9 -32.98',
            action='store', nargs='+', type=str, required=False)
    parser.add_argument('-vid', '--variable_id',
            help='The variable id youve chosen.',
            action='store', type=int, required=False)
    parser.add_argument('-sid', '--source_id',
            help='The source id youve chosen',
            action='store', type=int, required=False)
    parser.add_argument('-y', '--year',
            help='The year you want to check for data.',
            action='store', type=int, required=False)
    parser.add_argument('-m', '--month',
            help='The month you want to check for data.',
            action='store', type=int, required=False)
    args = parser.parse_args()
    location = args.location 
    variable_id = args.variable_id
    source_id = args.source_id
    year = args.year
    month = args.month
    model_point = args.model_point 
    if not any([location, variable_id, source_id, year, month, model_point]):
        main()
        
    if not location:
        if model_point:
            print('Check to see if a forecast exists for the model point')
            check_where_model_point(model_point)
            #check and return a path
        else:
            if variable_id:
                if source_id:
                    if not year or not month:
                        print('ERROR: Please specify the year and month you want to check for data.')
                    else:
                        print('Checking if variable_id {}, source_id {} on database..'.format(variable_id, source_id))
                        results = check_if_data_on_database(variable_id, source_id, year, month)
                        if results:
                            variable = []
                            dates = list(results.keys())
                            dates.sort()
                            for i in dates:
                                variable.append(results[i])
                            plot_database_data(dates, variable)
                else:
                    print('ERROR: You need to enter a source id aswell as a variable id.')
                    if not year or not month:
                        print('ERROR: Please specify the year and month you want to check for data.')
            else:
                if source_id:
                    print('ERROR: You need to enter a variable id aswell as a source id.')
                    if not year or not month:
                        print('ERROR: Please specify the year and month you want to check for data.')
    else:
        matches = get_site_id(location)
        get_variables(matches)





