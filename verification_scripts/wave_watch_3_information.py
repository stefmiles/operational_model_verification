
""" created by Stef 27/10/2017 for the wave watch 3 verifications. """

Models = ['GSM', 'APS', 'CMC', 'EC',
        'GFS', 'GFST2', 'GFS418',
        'NGM', 'WRF', 'SWAN']
Variables = ['hs', 'tp', 'dr']
Period = 12
Start_vp = 0
End_vp = Period *10
Rank = 72
Basins = ['SCS', 'BASSX', 'WA', 'TIMOR']
Wrf_basins = ['THAI_SCS418', 'NWS_WA418', 'TIMOR_GFS418']
Swan_basins = ['Kembla']


