#!/usr/bin/env python
'''
Standard OWS line styles for matplotlib lines plotting.
Contains a dictionary with model names as keys and color
and style as items.
Example use:

from ows_line_styles import line_styles
...
ax.plot(x, y, color=lineStyles[track.name].color,
              ls=lineStyles[track.name].style)
'''
#import matplotlib
#matplotlib.use('Agg')
from collections import namedtuple

LineStyle = namedtuple('LineStyle', ['color', 'style'])
line_styles = {
    'GFS': LineStyle('r', '-'),
    'GFST2': LineStyle('r','-'),
    'GFS418': LineStyle('r', '-'),
    'GFX':LineStyle('springgreen','-'),
    'GFSO': LineStyle('r', '-'),
    'NVGM': LineStyle('g', '--'),
    'NAVGEM': LineStyle('g', '--'),
    'NGM': LineStyle('lawngreen', '-'),
    'GSM': LineStyle('g', '-'),
    'CMC': LineStyle('b', '-'),
    'APS': LineStyle('#996633', '-'),
    'APS1': LineStyle('#996633', '-'),
    'APS2': LineStyle('#996633', '-'),
    'ACC': LineStyle('#996633', '-'),
    'EC': LineStyle('#5588FF', '-'),
    'ECMWF': LineStyle('#5588FF', '-'),
    'Mean Vector': LineStyle('k', '-'),
    'Mean Position': LineStyle('k', '--'),
    'WRF': LineStyle('m', '-'),
    'UKM': LineStyle('#FFA500', '-'),
    'ECEns': LineStyle('#33FFFF', '-'),
    
    'SWAN': LineStyle('gold', '-'),
    'ARW': LineStyle('darkorange', '-'),
    'HR': LineStyle('royalblue', '-'),
    'NMM': LineStyle('cyan', '-'),
    'ensemble': LineStyle('mediumslateblue','-'),
    'ensbc': LineStyle('y','--'),
    'ens': LineStyle('mediumslateblue','-'),
    
    'ARWd02': LineStyle('darkorange', '-'),
    'ARWd03': LineStyle('chocolate', '-'),
    'NMMd02': LineStyle('cyan', '-'),
    'NMMd03': LineStyle('darkcyan', '-'),
    'HRd02': LineStyle('royalblue', '-'),
    'HRd03': LineStyle('darkblue', '-')

}

