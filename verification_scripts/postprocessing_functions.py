
import os
import shutil
from owssendemail import send_mail

def email_results_to_stef(start_date, veri_type, locations, obs_path, vperiods, variables):
    to_list = ['stephanie@offshoreweather.com.au']
    subject = ('{} verification results'.format(veri_type))
    
    vp = vperiods[0]
    if veri_type != 'wrf':
        dt_increment = 12
    else:
        dt_increment = 6
    dir_list = []
    
    yr = start_date.strftime('%Y')
    yrmt = start_date.strftime('%Y%m')
    path_split = os.path.split(obs_path)
    up_save_path = path_split[0]
    path_split2 = os.path.split(up_save_path)
    veri_save_path = path_split2[0]
    
    locations_list = os.listdir(veri_save_path)
    #now this refers to all the locations successful in this verification type
    #for location in locations:
    for location in locations_list:
        for variable in variables:
            png_name = '{}_{}_{}-{}_timeseries.png'.format(location, 
                    variable, vp , vp+dt_increment)
            png_file = os.path.join(veri_save_path,location, yr, yrmt, variable, png_name)
            if os.path.isfile(png_file):
                dir_list.append(png_file)
            sum_name = 'summary_stats.txt'
            sum_file = os.path.join(veri_save_path,location, yr, yrmt, variable, sum_name)
            if os.path.isfile(sum_file):
                dir_list.append(sum_file)
    message = ('Stef, these are the latest model verification results ' 
            + 'for {}.'.format(start_date.strftime('%Y%m'))
            + '\n The locations should include {}'.format(locations_list))
    send_mail(to_list, subject, message, attachments=dir_list)





def annual_plots_vm_to_archive(obs_path, variables, veri_type,
            location):
    archive_verifications_path = os.path.join('/mnt/ows/Archive1/',
            'Verification', 'Model_Verifications', veri_type, location)
    # I CHOOSE THESE LOCATIONS JUST BASED ON WANTING TO LOOK AT A FEW RATHER
    # THAN ALL LOCATIONS.
    locations_saving = ['Arthit', 'Benchamas', 'Benchamas_fso', 'CapeSorell', 
            'CoffsHarbour', 'Erawan', 'Funan', 'Ichthys', 'MelbAirPort', 
            'Pailin', 'Platong', 'Platong_fso', 'PortKembla', 'Satun']
    if location in locations_saving:
        for variable in variables:
            folder_to_save_to = os.path.join(archive_verifications_path, variable)
            rank_summary_file = (location + '_' + variable + '_rank_ts.png')
            path_split = os.path.split(obs_path)
            upper_path = path_split[0]
            rank_summary_png_dir = os.path.join(upper_path, variable + '_summary', rank_summary_file)
            if os.path.isfile(rank_summary_png_dir):
                if os.path.isdir(folder_to_save_to):
                    try:
                        shutil.copy2(rank_summary_png_dir, 
                                os.path.join(folder_to_save_to, rank_summary_file))
                    except OSError:
                        os.remove(os.path.join(folder_to_save_to, rank_summary_file))
                        shutil.copy2(rank_summary_png_dir,
                            os.path.join(folder_to_save_to, rank_summary_file))
                else:
                    os.makedirs(folder_to_save_to)
                    shutil.copy2(rank_summary_png_dir,
                            os.path.join(folder_to_save_to, rank_summary_file))



def files_from_vm_to_archive(start_date, obs_path, variables, veri_type, location):
    archive_verifications_path = os.path.join('/mnt/ows/Archive1/',
            'Verification', 'Model_Verifications', veri_type, location)
    year = str(start_date.year)
    yeardate = str(start_date.strftime('%Y%m'))
    # I CHOOSE THESE LOCATIONS JUST BASED ON WANTING TO LOOK AT A FEW RATHER
    # THAN ALL LOCATIONS.
    locations_saving = ['Arthit', 'Benchamas', 'Benchamas_fso', 'CapeSorell', 
            'CoffsHarbour', 'Erawan', 'Funan', 'Ichthys', 'MelbAirPort', 
            'Pailin', 'Platong', 'Platong_fso', 'PortKembla', 'Satun']
    if location in locations_saving:
        for variable in variables:
            copying_list = []
            # save all to this dir
            folder_to_save_to = os.path.join(archive_verifications_path,
                    variable, yeardate)
            # 1. all the plots and the summary statistics
            plots_dir = os.path.join(obs_path, yeardate, variable)
            if os.path.isdir(plots_dir):
                if os.path.isfile(os.path.join(plots_dir,'summary_stats.txt')):
                    plots_exist=True
                else:
                    plots_exist=False
            else:
                plots_exist=False
            copying_list.append((plots_dir, plots_exist))
            # 2. the obs stats file 
            obs_stats_file = os.path.join(obs_path, 
                    location + '_Obs_Statistics_' + yeardate + '.txt')
            if os.path.isfile(obs_stats_file):
                obs_stats_exists = True
            else:
                obs_stats_exists = False
            copying_list.append((obs_stats_file, obs_stats_exists))
            for item, exist in copying_list:
                if exist:
                    if os.path.isdir(folder_to_save_to):
                        try:
                            shutil.copytree(item, folder_to_save_to)
                        except NotADirectoryError:
                            shutil.copy2(item, folder_to_save_to)
                        except OSError:
                            try:
                                shutil.rmtree(folder_to_save_to)
                            except:
                                print('failed at rming')
                            shutil.copytree(item, folder_to_save_to)
                    else:
                        try:
                            shutil.copytree(item, folder_to_save_to)
                        except NotADirectoryError:
                            os.makedirs(folder_to_save_to)
                            shutil.copy2(item, folder_to_save_to)



