from datetime import datetime
from dateutil.relativedelta import relativedelta
from calendar import monthrange

import verification_top_class as vtp
veri_types = [
        'wave_watch_3',
        #'global_winds',
        #'wrf',
        #'wave_watch_3_outlook_5_days',
        #'wave_watch_3_outlook_10_days'
        ]

#veri_types = ['wave_watch_3']
veri_types = ['global_winds']
for vt in veri_types:
    veri_type = [vt]
    print('THE VERI TYPE {}'.format(veri_type))
    start_date = datetime(2017,6,1,0)
    end_date = datetime(2017,6,30,23)
    month_list = []
    while start_date <= end_date:
        the_year = start_date.year
        the_month = start_date.month
        month_list.append(datetime(the_year, the_month, 1))
        start_date = start_date + relativedelta(months=1)
    
    for the_month in month_list:
        last_day = monthrange(the_month.year, the_month.month)[1]
        end_date = datetime(the_month.year, the_month.month, last_day, 23)
        mm = vtp.Verification(start_date=the_month, end_date=end_date, 
            verification_types=veri_type, base_dir='Dev', custom_use=True)    
        mm.run_a_verification()

