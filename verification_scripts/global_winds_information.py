""" Created by Stef 13/11/2017 for global wind veris. """
Models = ['APS','CMC','EC', 'GFS418','NGM']
Variables = ['wspd','wdir']
Period = 12
Start_vp = 0
End_vp = Period *10
Rank = 72
Basins = ['SCS', 'BASSX', 'WA', 'TIMOR']
Wrf_basins = ['THAI_SCS418', 'NWS_WA418', 'TIMOR_GFS418']


