
import os

import numpy as np
import scipy.stats
from datetime import datetime, timedelta

class Statistics(object):
    """ This is my statistics class."""
    def __init__(self, start_date, end_date, location, 
            observations_instance, save_path,
            model_dictionary={}, rank_period=72,
            verification_type=None, variables=['hs'], 
            models=['EC'], verification_periods=[0]):
        """ This is the init doumentation."""
        self.start_date = start_date
        self.end_date = end_date
        self.location = location 
        self.observations_instance = observations_instance
        self.save_path = save_path
        self.rank_period = rank_period
        if verification_type:
            self.veri_type = verification_type
            if self.veri_type != 'wrf':
                self.dt_increment = 12
                self.model_freq = 8
            else:
                self.dt_increment = 6
                self.model_freq = 24
        self.variables = variables
        self.models = models
        if model_dictionary:
            self.model_dictionary = model_dictionary
        if len(verification_periods) >= 0:
            self.verification_periods = verification_periods

    def write_line(self, title, value):
        line2write = title.rjust(20)
        line2write += '{}'.format(value).rjust(10)
        self.oid.write(line2write + '\n')

    def round_value(self, data):
        return round(data, 2)

    def calculate_and_write_observations_statistics(self):
        """ calculate and write the statitsics for each variable to a txt file.
        """
        file_date = self.start_date.strftime('%Y%m')
        file_title = '{}_Obs_Statistics_{}.txt'.format(self.location, file_date)
        file_to_open = os.path.join(self.save_path, file_title)
        self.oid = open(file_to_open, 'w')
        with self.oid:
            self.oid.write('{} observations statistics '.format(self.location)
                    + 'for {}. \n'.format(file_date))
            self.oid.write('Please note, statistics are calculated on raw obs'
                    + ' data which could be in any time interval (seconds,'
                    + ' half hourly or even hourly).\n')
            for variable in self.variables:
                time_array = getattr(self.observations_instance,
                        'raw_time_array')
                variable_array = getattr(self.observations_instance, 
                        'raw_' + variable + '_array')
                ob_data_freq = self.det_obs_data_freq(time_array)
                expected_data_size = (time_array[-1].day) * ob_data_freq
                
                self.oid.write('{} variable statistics: \n'.format(variable))
                ob_min = self.round_value(np.nanmin(variable_array))
                self.write_line('min', ob_min)
                ob_med = self.round_value(np.nanmedian(variable_array))
                self.write_line('med', ob_med)
                ob_max = self.round_value(np.nanmax(variable_array))
                self.write_line('max', ob_max)
                ob_mean = self.round_value(np.nanmean(variable_array))
                self.write_line('mean', ob_mean)
                ob_range = self.round_value(ob_max - ob_min)
                self.write_line('range', ob_range)
                ob_std = self.round_value(np.nanstd(variable_array))
                self.write_line('std_dev', ob_std)
                ob_var = self.round_value(np.nanvar(variable_array))
                self.write_line('variance', ob_var)
                ob_size = (len(np.ma.masked_invalid(variable_array)))
                self.write_line('data_size', ob_size)
                ob_obt = int((ob_size / expected_data_size) * 100)
                self.write_line('obs_obtained', ob_obt)
                self.oid.write('\n')


    def det_obs_data_freq(self, time_array):
        # the diff[1] is how many data in a 24 hr period. 
        # if the data is every minute, then the diff[1] is 
        # 60*24 = 1440 
        diff_options = [
                (1, (60*24)), 
                (10, (6*24)), 
                (30, (2*24)),
                (60, (1*24)), 
                (180, (24/3))]
        first_time = time_array[0]
        second_time = time_array[1]
        third_time = time_array[2]
        diff1 = ((second_time - first_time).total_seconds())/60
        diff2 = ((third_time - second_time).total_seconds())/60
        if diff2 == diff1:
            diff = {}
            for d_opt in diff_options:
                time_diff = (abs(diff1 - d_opt[0]))
                diff[time_diff] = d_opt[0]
            min_diff = np.min(list(diff.keys()))
            freq = (24*60) / (diff[min_diff])
        else:
            #print('not the same!')
            diff = {}
            for d_opt in diff_options:
                time_diff = (abs(diff1 - d_opt[0]))
                diff[time_diff] = d_opt[0]
            min_diff = np.min(list(diff.keys()))
            freq = (24*60) / (diff[min_diff])
        return freq

    def mask_two_arrays(self, obs_unmasked, model_unmasked):
        obs_mask = ~np.isnan(obs_unmasked)
        model_mask =~np.isnan(model_unmasked)
        full_mask = obs_mask * model_mask 
        obs_array = obs_unmasked[full_mask]
        model_array = model_unmasked[full_mask]
        return obs_array, model_array
    
    def mask_three_arrays(self, time_unmasked, obs_unmasked, model_unmasked):
        obs_mask = ~np.isnan(obs_unmasked)
        model_mask =~np.isnan(model_unmasked)
        full_mask = obs_mask * model_mask 
        obs_array = obs_unmasked[full_mask]
        model_array = model_unmasked[full_mask]
        time_array = time_unmasked[full_mask]
        return time_array, obs_array, model_array

    def get_obs_stats(self, variable, stat):
        file_date = self.start_date.strftime('%Y%m')
        file_title = '{}_Obs_Statistics_{}.txt'.format(self.location, file_date)
        file_to_open = os.path.join(self.save_path, file_title)
        try:
            oid = open(file_to_open, 'r')
        except:
            print('Obs file couldnt be opened when calculating stats.')
            import ipdb;ipdb.set_trace()
            return 100
        with oid:
            for line in oid:
                if line.count(variable):
                    found_var = True
                if line.count('{}'.format(stat)):
                    obs_stat = line.split()[1]
                    return float(obs_stat)
        print('Didnt find obs statistic')
        return 100

    def model_line(self, line2write, addition, rjust):
            if type(addition) != str:
                addition = round(addition, 2)
            line2write += '{}'.format(addition).rjust(rjust)
            return line2write


    def calculate_and_write_wrf_summary_statistics(self):
        file_date = self.start_date.strftime('%Y%m')
        file_title = 'summary_stats.txt'
        models = self.model_dictionary.keys()
        for variable in self.variables:
            #print('Doing stats for {}'.format(variable))
            total_rank_dict = {}
            obs_percent = self.get_obs_stats(variable, 'obtained')
            obs_std = self.get_obs_stats(variable, 'std')
            obs_mean = self.get_obs_stats(variable, 'mean')
            file_to_open = os.path.join(self.save_path, file_date, 
                    variable, file_title)
            sid = open(file_to_open, 'w')
            with sid:
                sid.write('{} {} {} summary statistics \n'.format(
                    self.location, file_date, variable))
                sid.write('Percent of observations: {} \n \n'.format(int(obs_percent)))
                for vp in self.verification_periods:
                    #print('DOING THIS VP {}'.format(vp))
                    sid.write('{}:{} hour verification period \n'.format(vp, 
                        vp+self.dt_increment))
                    if variable == 'rain':
                        #print('DOING INITIAL RAIN STATS')
                        self.calculate_initial_rain_statistics(models, vp, sid)
                    else:
                        total_rank_dict = self.calculate_non_rain_statistics(total_rank_dict, obs_std, obs_mean, variable, sid, vp, models)
                for vp in self.verification_periods:
                    if variable != 'rain':
                        continue
                    sid.write('{}:{} hour verification period \n'.format(vp,
                        vp+self.dt_increment))
                    self.calculate_second_rain_statistics(models, vp, sid)
            if variable == 'rain':
                continue
            sid = open(file_to_open, 'a')
            with sid:
                sid.write('\n')
                sid.write('Models total ranks for 0-{} hours \n'.format(self.rank_period))
                for model in total_rank_dict:
                    no_vp = len(total_rank_dict[model])
                    mod_sum = np.nansum(total_rank_dict[model])
                    mod_rank = round(mod_sum / no_vp, 2)
                    sid.write('{} {} \n'.format(model, mod_rank)) 


            #import ipdb;ipdb.set_trace()


    def calculate_non_rain_statistics(self, total_rank_dict, obs_std, obs_mean, variable, sid, vp, models):
        sid.write('Models'.rjust(12) + 'mae'.rjust(12) 
                + 'bias'.rjust(12) + 'rmse'.rjust(12) 
                + 'rmse_rank'.rjust(12) + 'rr'.rjust(12) 
                + 's_index'.rjust(12) + 'r_index'.rjust(12) 
                + 'rank'.rjust(12) + '%data_avail'.rjust(13)
                + 'hit_percent'.rjust(15) 
                #+ 'hit_simple(%)'.rjust(13) + 'hit_bins(%)'.rjust(13) 
                + '\n')
        for model in models:
            if vp in self.model_dictionary[model]:
                model_var = getattr(self.model_dictionary[model][vp], 
                        '{}_array'.format(variable))
                obs_var = getattr(self.observations_instance, 
                        '{}_array'.format(variable))
                obs_array, model_array = self.mask_two_arrays(
                        obs_var, model_var)
                if len(model_array) > 0:
                    line2write = ''
                    line2write = self.model_line(line2write, model, 12)
                    mae = self.calc_mae(obs_array, model_array)
                    line2write = self.model_line(line2write, mae, 12)
                    bias = self.calc_bias(obs_array, model_array)
                    line2write = self.model_line(line2write, bias, 12)
                    rmse = self.calc_rmse(obs_array, model_array)
                    line2write = self.model_line(line2write, rmse, 12)
                    rmse_rank = self.calc_rmse_rank(rmse, obs_std)
                    line2write = self.model_line(line2write, rmse_rank, 12)
                    rr = self.calc_lin_reg(obs_array, model_array)
                    line2write = self.model_line(line2write, rr, 12)
                    si = self.calc_s_index(rmse, obs_mean)
                    line2write = self.model_line(line2write, si, 12)
                    ri = self.calc_r_index(rmse, obs_std)
                    line2write = self.model_line(line2write, ri, 12)
                    rank = self.calc_rank(rmse_rank, rr)
                    line2write = self.model_line(line2write, rank, 12)
                    data_avail = self.calc_model_avail(model_var, model_array)
                    line2write = self.model_line(line2write, data_avail, 13)
                    hit_score = self.calc_hitmiss_score(variable, obs_array, model_array)
                    #hit_simple, hit_bins = self.calc_hit_miss(obs_array, model_array)
                    line2write = self.model_line(line2write, hit_score, 15)
                    #line2write = self.model_line(line2write, hit_bins, 13)
                    line2write = self.model_line(line2write, '\n',1 )
                    sid.write(line2write)
                    if vp <= self.rank_period:
                        if model not in total_rank_dict:
                            total_rank_dict[model] = []
                        total_rank_dict[model].append(rank)
        return total_rank_dict
        

    def calculate_initial_rain_statistics(self, models, vp, sid):
        variable = 'rain'
        if 'GFS_rain' in models:
            print('GFS WAS IN')
            acc_hr = 3
        else:
            print('GFS NOT IN')
            acc_hr = 1
        dates = []
        the_start = self.start_date
        the_end = self.end_date
        while the_start<the_end:
            dates.append(the_start)
            the_start = the_start +timedelta(hours=acc_hr)
        # dont need to reaccululate, hrly data is fine.
        sid.write('Models'.rjust(13) 
                    + 'points<0.3mm'.rjust(15)
                    + 'points>0.3mm'.rjust(15)
                    + 'match_points'.rjust(13)
                    + 'total_points'.rjust(13)
                    + 'percent'.rjust(13)
                    + '\n')
        for model in models:
            #print('THIS IS THE MODEL'.format(model))
            if model.count('ens'):# == 'ensemble':
                continue
            elif model.count('GFS'):
                time_var = dates
                model_var = getattr(self.model_dictionary[model][vp],
                        '{}_array'.format(variable))
            else:
                #print('THIS IS THE MODEL {} AND ENTERING REACCUL'.format(model))
                #print('IM IN INITIAL')
                time_var, model_var = self.reaccumulate_rain_data(model, vp, dates)
            obs_var = self.reaccumulate_obs_rain_data(dates)
            obs_array, model_array = self.mask_two_arrays(obs_var, model_var)
            if len(model_array):
                points_above = 0
                points_below = 0
                for i, j in zip(obs_array, model_array):
                    if i > 0.3 and j > 0.3:
                        points_above +=1
                    if i < 0.3 and j < 0.3:
                        points_below +=1
                points_together = points_above + points_below
                no_points = len(obs_array)
                try:
                    percent = int((points_together/no_points)*100)
                except:
                    import ipdb;ipdb.set_trace()
                sid.write(str(model).rjust(13)
                        + str(points_below).rjust(15)
                        + str(points_above).rjust(15)
                        + str(points_together).rjust(13)
                        + str(no_points).rjust(13)
                        + str(percent).rjust(13)
                        + '\n')
    
    def calculate_second_rain_statistics(self, models, vp, sid):
            
        variable = 'rain'
        if 'GFS_rain' in models:
            acc_hr = 3
        else:
            acc_hr = 1
        dates = []
        the_start = self.start_date
        the_end = self.end_date
        while the_start<the_end:
            dates.append(the_start)
            the_start = the_start +timedelta(hours=acc_hr)
        sid.write('Models'.rjust(15) 
                + 'no_obs'.rjust(8)
                + 'no_mods'.rjust(8)
                + 'hits'.rjust(5)
                + 'misses'.rjust(7)
                + 'false_alarms'.rjust(13)
                + 'correct_negs'.rjust(15)
                + 'accuracy'.rjust(9)
                + 'bias'.rjust(7)
                + 'POD'.rjust(6)
                + 'FAR'.rjust(6)
                + 'SR'.rjust(5)
                + '\n')
        rain_ranges = [('<0.3',0,0.3),
                ('0.3-1mm', 0.3, 1), 
                ('1-3mm',1, 3), 
                ('3-10mm', 3, 10), 
                ('>10mm', 10, 1000)]  
        for rain_bin in rain_ranges:
            sid.write('Rainfall bin {}'.format(rain_bin[0]) + '\n')
            for model in models:
                if model.count('ens'):#emble':
                    continue
                elif model.count('GFS'):
                    model_var = getattr(self.model_dictionary[model][vp],
                            '{}_array'.format(variable))
                else:
                    #print('IM IN SECOND ONE....')
                    time_var, model_var = self.reaccumulate_rain_data(model, vp, dates)
                obs_var = self.reaccumulate_obs_rain_data(dates)
                obs_array, model_array = self.mask_two_arrays(obs_var, model_var)
                if len(list(model_array)):
                    line2write = ''
                    line2write = self.model_line(line2write, model, 15)
                    obs_in, model_in, hits, misses, false_alarms, correct_negs = self.calc_ctable(rain_bin, obs_array, model_array)
                    line2write = self.model_line(line2write, obs_in, 8)
                    line2write = self.model_line(line2write, model_in, 8)
                    line2write = self.model_line(line2write, hits, 5)
                    line2write = self.model_line(line2write, misses, 7)
                    line2write = self.model_line(line2write, false_alarms, 13)
                    line2write = self.model_line(line2write, correct_negs, 15)
                    accuracy = self.calc_accuracy(model_array, hits, correct_negs)
                    line2write = self.model_line(line2write, accuracy, 9)
                    rain_bias = self.calc_rain_bias(hits, false_alarms, misses)
                    line2write = self.model_line(line2write, rain_bias, 7)
                    pod = self.calc_pod(hits, misses)
                    line2write = self.model_line(line2write, pod, 6)
                    far = self.calc_far(hits, false_alarms)
                    line2write = self.model_line(line2write, far, 6)
                    sr = self.calc_sr(hits, false_alarms)
                    line2write = self.model_line(line2write, sr, 5)
                    line2write = self.model_line(line2write, '\n', 1)
                    sid.write(line2write)

    def calc_ctable(self, rain_bin, obs_array, model_array):
        """ Calculates contingency table like stats for rain data."""
        bin_min = rain_bin[1]
        bin_max = rain_bin[2]
        hits = 0
        misses = 0
        false_alarms = 0
        correct_negatives = 0
        my_len = len(model_array)
        obs_in = 0
        model_in = 0
        if my_len != 0:
            for index, val in enumerate(obs_array):
                obs_index = index
                obs_val = obs_array[index]
                model_val = model_array[index]
                if obs_val > bin_min and obs_val < bin_max:
                    obs_in = obs_in + 1
                    if model_val > bin_min and model_val <bin_max:
                        model_in = model_in + 1
                        hits = hits + 1
                    else:
                        misses = misses + 1
                else:
                    if model_val > bin_min and model_val < bin_max:
                        model_in = model_in + 1
                        false_alarms = false_alarms + 1
                    else:
                        correct_negatives = correct_negatives + 1
        return obs_in, model_in, hits, misses, false_alarms, correct_negatives
    
    def calc_accuracy(self, model_array, hits, correct_negatives):
        my_len = len(model_array)
        accuracy = ((hits + correct_negatives)/my_len) * 100
        return round(accuracy, 0)

    def calc_rain_bias(self, hits, false_alarms, misses):
        if hits + misses != 0:
            bias = ((hits + false_alarms)/(hits + misses))
            return round(bias, 2)
        else:
            return np.nan

    def calc_pod(self, hits, misses):
        if hits + misses != 0:
            pod = round(((hits / (hits + misses))*100),0)
            return int(pod)
        else:
            return np.nan
    
    def calc_far(self, hits, false_alarms):
        if hits + false_alarms != 0:
            false_alarm_ratio = round((false_alarms / (hits + false_alarms))*100,0)
            return int(false_alarm_ratio)
        else:
            return np.nan

    def calc_sr(self, hits, false_alarms): 
        if hits + false_alarms != 0:
            success_ratio = round((hits/(hits + false_alarms))*100,0)
            return int(success_ratio)
        else:
            return np.nan
            

    def reaccumulate_rain_data(self, model, vp, dates):
        variable = 'rain'
        var_list = []
        dates.sort()
        try:
            model_var = getattr(self.model_dictionary[model][vp],
                    '{}_array'.format(variable))
        except:
            model_var = []
        #print('IM IN RAIN DATA FUNCTION {}, {}'.format(model, vp))

        try:
            orig_time_array = self.model_dictionary[model][vp].time_array
        except:
            import ipdb;ipdb.set_trace()
        for index, val in enumerate(dates):
            dt = val
            #print(dt)
            if index == 0:
                acc_rain = np.nan
                prev_dt = dt
            else:
                prev_dt = dates[index-1]
            acc_rain = [] 
            for raw_dt in orig_time_array:
                if raw_dt <= dt and raw_dt > prev_dt:
                    time_list = list(orig_time_array)
                    rain_index = time_list.index(raw_dt)
                    rain_val = model_var[rain_index]
                    acc_rain.append(rain_val)
            #print('What found: {}'.format(acc_rain))
            acc_init = np.array(acc_rain)
            mask = np.isfinite(acc_init)
            acc_array = acc_init[mask]
            #print('What left: {}'.format(acc_array))
            if len(acc_array):
                acc_val = 0
                for i in acc_array:
                    acc_val = acc_val + i
            else:
                acc_val = np.nan
            var_list.append(acc_val)
            #print('What added: {}'.format(acc_val))
        #import ipdb;ipdb.set_trace()
        return np.array(dates), np.array(var_list)

    def reaccumulate_obs_rain_data(self, dates):
        variable = 'rain'
        new_list = []
        dates.sort()
        obs_var = self.observations_instance.rain_array
        orig_time_array = self.observations_instance.time_array
        for index, val in enumerate(dates):
            dt = val
            obs_vals  = []
            if index == 0:
                acc_rain = np.nan
                prev_dt = dt
            else:
                prev_dt = dates[index-1]
            acc_rain = 0 
            for raw_dt in orig_time_array:
                if raw_dt <= dt and raw_dt > prev_dt:
                    time_list = list(orig_time_array)
                    rain_index = time_list.index(raw_dt)
                    rain_val = obs_var[rain_index]
                    obs_vals.append(rain_val)
            acc_init = np.array(obs_vals)
            mask = np.isfinite(acc_init)
            acc_array = acc_init[mask]
            if len(acc_array):
                acc_val = 0
                for i in acc_array:
                    acc_val = acc_val + i
            else:
                acc_val = np.nan
            new_list.append(acc_val)
        return np.array(new_list)


    def calculate_and_write_bias_stats(self):
        models = self.model_dictionary.keys()
        if self.start_date.month < self.end_date.month:
            file_date = '{}-{}'.format(self.start_date.strftime('%Y%m'), self.end_date.strftime('%Y%m'))
        else:
            file_date = self.start_date.strftime('%Y%m')
        for variable in self.variables:
            if variable != 'hs':
                continue
            file_title = '{}_{}.txt'.format(file_date, variable)
            total_rank_dict = {}
            obs_percent = self.get_obs_stats(variable, 'obtained')
            
            obs_std = self.get_obs_stats(variable, 'std')
            obs_mean = self.get_obs_stats(variable, 'mean')
            
            save_path = os.path.join('/mnt/fs02/fs02/model/Ocean/WW3/', 
                    'bias_stats', self.location)
            if not os.path.isdir(save_path):
                os.makedirs(save_path)
            file_to_open = os.path.join(save_path, file_title)
            with open(file_to_open, 'w') as sid:
                sid.write('{} {} {} Bias statistics \n'.format(
                    self.location, file_date, variable))
                sid.write('Percent of observations: {} \n \n'.format(int(obs_percent)))
                for vp in self.verification_periods:
                    sid.write('{}:{} hour verification period \n'.format(vp, 
                        vp+self.dt_increment))
                    sid.write('Models'.rjust(10) 
                            + 'count'.rjust(10) 
                            + 'bias'.rjust(10) 
                            + 'mae'.rjust(10)
                            + 'rmse'.rjust(10)
                            + 'min'.rjust(10)
                            + 'median'.rjust(10)
                            + 'max'.rjust(10)
                            + '\n')
                    for model in models:
                        if vp in self.model_dictionary[model]:
                            model_var = getattr(self.model_dictionary[model][vp], 
                                    '{}_array'.format(variable))
                            obs_var = getattr(self.observations_instance, 
                                    '{}_array'.format(variable))
                            obs_array, model_array = self.mask_two_arrays(
                                    obs_var, model_var)
                            size = len(obs_array)
                            if len(model_array) > 0:
                                line2write = ''
                                line2write = self.model_line(line2write, model, 10)
                                line2write = self.model_line(line2write, size, 10)
                                bias = self.calc_bias(obs_array, model_array)
                                line2write = self.model_line(line2write, bias, 10)
                                mae = self.calc_mae(obs_array, model_array)
                                line2write = self.model_line(line2write, mae, 10)
                                rmse = self.calc_rmse(obs_array, model_array)
                                line2write = self.model_line(line2write, rmse, 10)
                                err_min = self.calc_error_min(obs_array, model_array)
                                line2write = self.model_line(line2write, err_min, 10)
                                err_med = self.calc_error_med(obs_array, model_array)
                                line2write = self.model_line(line2write, err_med, 10)
                                err_max = self.calc_error_max(obs_array, model_array)
                                line2write = self.model_line(line2write, err_max, 10)
                                line2write = self.model_line(line2write, '\n',1 )
                                sid.write(line2write)


    def calculate_and_write_summary_statistics(self):
        file_date = self.start_date.strftime('%Y%m')
        file_title = 'summary_stats.txt'
        models = self.model_dictionary.keys()
        for variable in self.variables:
            total_rank_dict = {}
            obs_percent = self.get_obs_stats(variable, 'obtained')
            obs_std = self.get_obs_stats(variable, 'std')
            obs_mean = self.get_obs_stats(variable, 'mean')
            file_to_open = os.path.join(self.save_path, file_date, 
                    variable, file_title)
            sid = open(file_to_open, 'w')
            with sid:
                sid.write('{} {} {} summary statistics \n'.format(
                    self.location, file_date, variable))
                sid.write('Percent of observations: {} \n \n'.format(int(obs_percent)))

                for vp in self.verification_periods:
                    sid.write('{}:{} hour verification period \n'.format(vp, 
                        vp+self.dt_increment))
                    sid.write('Models'.rjust(20) + 'mae'.rjust(20) 
                            + 'bias'.rjust(20) + 'rmse'.rjust(20) 
                            + 'rmse_rank'.rjust(20) + 'rr'.rjust(20) 
                            + 's_index'.rjust(20) + 'r_index'.rjust(20) 
                            + 'rank'.rjust(20) + '%data_avail'.rjust(20)
                            + 'hit_percent'.rjust(20)  
                            #+ 'hit_simple(%)'.rjust(20) + 'hit_bins(%)'.rjust(20) 
                            + '\n')
                    for model in models:
                        if vp in self.model_dictionary[model]:
                            model_var = getattr(self.model_dictionary[model][vp], 
                                    '{}_array'.format(variable))
                            obs_var = getattr(self.observations_instance, 
                                    '{}_array'.format(variable))
                            obs_array, model_array = self.mask_two_arrays(
                                    obs_var, model_var)
                            if len(model_array) > 0:
                                line2write = ''
                                line2write = self.model_line(line2write, model, 20)
                                mae = self.calc_mae(obs_array, model_array)
                                line2write = self.model_line(line2write, mae, 20)
                                bias = self.calc_bias(obs_array, model_array)
                                line2write = self.model_line(line2write, bias, 20)
                                rmse = self.calc_rmse(obs_array, model_array)
                                line2write = self.model_line(line2write, rmse, 20)
                                rmse_rank = self.calc_rmse_rank(rmse, obs_std)
                                line2write = self.model_line(line2write, rmse_rank, 20)
                                rr = self.calc_lin_reg(obs_array, model_array)
                                line2write = self.model_line(line2write, rr, 20)
                                si = self.calc_s_index(rmse, obs_mean)
                                line2write = self.model_line(line2write, si, 20)
                                ri = self.calc_r_index(rmse, obs_std)
                                line2write = self.model_line(line2write, ri, 20)
                                rank = self.calc_rank(rmse_rank, rr)
                                line2write = self.model_line(line2write, rank, 20)
                                data_avail = self.calc_model_avail(model_var, model_array)
                                line2write = self.model_line(line2write, data_avail, 20)
                                hit_score = self.calc_hitmiss_score(variable, obs_array, model_array)
                                #hit_simple, hit_bins = self.calc_hit_miss(obs_array, model_array)
                                line2write = self.model_line(line2write, hit_score, 20)
                                #line2write = self.model_line(line2write, hit_bins, 20)
                                line2write = self.model_line(line2write, '\n',1 )
                                sid.write(line2write)
                                if vp >= self.rank_period:
                                    if model not in total_rank_dict:
                                        total_rank_dict[model] = []
                                    total_rank_dict[model].append(rank)
                    sid.write('\n')
                sid.write('Models total ranks for 0-{} hours \n'.format(self.rank_period))
                for model in total_rank_dict:
                    no_vp = len(total_rank_dict[model])
                    mod_sum = np.nansum(total_rank_dict[model])
                    mod_rank = round(mod_sum / no_vp, 2)
                    sid.write('{} {} \n'.format(model, mod_rank)) 



    def calc_mae(self, obs_array, model_array):
        num = 1/len(model_array)
        abs_diff = abs(model_array - obs_array)
        mae = np.nansum(num * abs_diff)
        return mae

    def calc_bias(self, obs_array, model_array):
        num = 1/len(model_array)
        sum_diff = np.nansum(model_array - obs_array)
        bias = num*sum_diff
        return bias
    
    def calc_rmse(self, obs_array, model_array):
        num = 1/len(model_array)
        diff = model_array - obs_array
        diff_sqrd = diff **2
        sqr_sum_diff = np.nansum(num*diff_sqrd)
        rmse = np.sqrt(sqr_sum_diff)
        return rmse
    
    def calc_error_min(self, obs_array, model_array):
        diff = model_array - obs_array
        err_min = np.nanmin(diff)
        return err_min

    def calc_error_med(self, obs_array, model_array):
        diff = model_array - obs_array
        err_med = np.nanmedian(diff)
        return err_med
    
    def calc_error_max(self, obs_array, model_array):
        diff = model_array - obs_array
        err_max = np.nanmax(diff)
        return err_max

    def calc_rmse_rank(self, rmse, obs_std, passing_score=0.5):
        try:
            div = rmse/obs_std
            div_sqr = div**2
            rmse_rank = passing_score ** div_sqr
        except:
            rmse_rank = np.nan
        return rmse_rank
    
    def calc_lin_reg(self, obs_array, model_array):
        slope, intercept, r_value, p_value, std_err = (
            scipy.stats.linregress(obs_array, model_array))
        rr = r_value*r_value
        return rr
    
    def calc_s_index(self, rmse, obs_mean):
        try:
            si = rmse/obs_mean 
        except:
            si = np.nan
        return si
    
    def calc_r_index(seld, rmse, obs_std):
        try:
            ri = rmse/obs_std
        except:
            ri = np.nan
        return ri
    
    def calc_rank(self, rmse_rank, rr):
        rank = (rmse_rank + rr)/2.
        return rank 
    
    def calc_model_avail(self, raw_model_array, masked_model_array):
        act_len = len(raw_model_array)
        real_len = len(masked_model_array)
        percent = (real_len/act_len)*100
        return int(percent)


    def define_variable_bins(self, variable):
        if variable == 'tp':
            lower = 7
            upper = 20
            criteria_out = 3
            criteria_in = 2
        elif variable == 'dr' or variable == 'wdir':
            lower = 0
            upper = 360
            criteria_out = 15 
            criteria_in = 20
        elif variable == 'hs':
            lower = 0
            upper = 20
            criteria_out = 0.5 
            criteria_in = 0.3
        elif variable == 'wspd':
            lower = 10
            upper = 20
            criteria_out = 6 
            criteria_in = 5
        elif variable == 'temp':
            lower = 0
            upper = 50
            criteria_out = 5 
            criteria_in = 3 
        else:
            lower = np.nan
            upper = np.nan
            criteria_out = np.nan 
            criteria_in = np.nan
            print('The hit miss criteria for this variable: {} hasnt been specified.')
        return lower, upper, criteria_out, criteria_in

    def calc_hitmiss_score(self, variable, obs_array, model_array):
        lower, upper, criteria_out, criteria_in = self.define_variable_bins(variable)
        out_count = 0
        out_hit = 0
        in_count = 0 
        in_hit = 0
        for idx, ob in enumerate(obs_array):
            if ob < lower or ob > upper:
                out_count += 1
                mod = model_array[idx]
                the_diff = abs(ob - mod)
                if the_diff <= criteria_out:
                    out_hit +=1
            else:
                in_count +=1
                mod = model_array[idx]
                the_diff = abs(ob - mod)
                if the_diff <= criteria_in:
                    in_hit +=1
        try:
            score = int(((out_hit + in_hit)/(out_count + in_count)) *100)
        except:
            score = np.nan
        return score

                



